SELECT SETVAL('admin_menu_id_seq', (SELECT max(id) FROM admin_menu));
SELECT SETVAL('admin_operation_log_id_seq', (SELECT max(id) FROM admin_operation_log));
SELECT SETVAL('admin_permissions_id_seq', (SELECT max(id) FROM admin_permissions));
SELECT SETVAL('admin_roles_id_seq', (SELECT max(id) FROM admin_roles));
SELECT SETVAL('admin_users_id_seq', (SELECT max(id) FROM admin_users));
SELECT SETVAL('advert_page_blocks_id_seq', (SELECT max(id) FROM advert_page_blocks));
SELECT SETVAL('advert_products_id_seq', (SELECT max(id) FROM advert_products));
SELECT SETVAL('advertising_categories_id_seq', (SELECT max(id) FROM advertising_categories));
SELECT SETVAL('advertising_filters_id_seq', (SELECT max(id) FROM advertising_filters));
SELECT SETVAL('advertisings_id_seq', (SELECT max(id) FROM advertisings));
SELECT SETVAL('brands_id_seq', (SELECT max(id) FROM brands));
SELECT SETVAL('cart_id_seq', (SELECT max(id) FROM cart));
SELECT SETVAL('categories_id_seq', (SELECT max(id) FROM categories));
SELECT SETVAL('category_filters_id_seq', (SELECT max(id) FROM category_filters));
SELECT SETVAL('company_info_id_seq', (SELECT max(id) FROM company_info));
SELECT SETVAL('departure_points_id_seq', (SELECT max(id) FROM departure_points));
SELECT SETVAL('discount_levels_id_seq', (SELECT max(id) FROM discount_levels));
SELECT SETVAL('failed_jobs_id_seq', (SELECT max(id) FROM failed_jobs));
SELECT SETVAL('files_id_seq', (SELECT max(id) FROM files));
SELECT SETVAL('filter_values_id_seq', (SELECT max(id) FROM filter_values));
SELECT SETVAL('filters_id_seq', (SELECT max(id) FROM filters));
SELECT SETVAL('footer_id_seq', (SELECT max(id) FROM footer));
SELECT SETVAL('incoming_products_id_seq', (SELECT max(id) FROM incoming_products));
SELECT SETVAL('item_departure_points_id_seq', (SELECT max(id) FROM item_departure_points));
SELECT SETVAL('item_shop_order_id_seq', (SELECT max(id) FROM item_shop_order));
SELECT SETVAL('items_id_seq', (SELECT max(id) FROM items));
SELECT SETVAL('items_count_id_seq', (SELECT max(id) FROM items_count));
SELECT SETVAL('language_lines_id_seq', (SELECT max(id) FROM language_lines));
SELECT SETVAL('menu_id_seq', (SELECT max(id) FROM menu));
SELECT SETVAL('menu_items_id_seq', (SELECT max(id) FROM menu_items));
SELECT SETVAL('migrations_id_seq', (SELECT max(id) FROM migrations));
SELECT SETVAL('orders_id_seq', (SELECT max(id) FROM orders));
SELECT SETVAL('personal_access_tokens_id_seq', (SELECT max(id) FROM personal_access_tokens));
SELECT SETVAL('product_categories_id_seq', (SELECT max(id) FROM product_categories));
SELECT SETVAL('product_filter_values_id_seq', (SELECT max(id) FROM product_filter_values));
SELECT SETVAL('product_filters_id_seq', (SELECT max(id) FROM product_filters));
SELECT SETVAL('product_names_id_seq', (SELECT max(id) FROM product_names));
SELECT SETVAL('product_properties_id_seq', (SELECT max(id) FROM product_properties));
SELECT SETVAL('products_id_seq', (SELECT max(id) FROM products));
SELECT SETVAL('properties_id_seq', (SELECT max(id) FROM properties));
SELECT SETVAL('review_likes_id_seq', (SELECT max(id) FROM review_likes));
SELECT SETVAL('reviews_id_seq', (SELECT max(id) FROM reviews));
SELECT SETVAL('shop_order_id_seq', (SELECT max(id) FROM reviews));
SELECT SETVAL('socials_id_seq', (SELECT max(id) FROM socials));
SELECT SETVAL('users_id_seq', (SELECT max(id) FROM users));
