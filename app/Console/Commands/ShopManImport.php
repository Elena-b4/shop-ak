<?php

namespace App\Console\Commands;

use App\Modules\Database\src\Models\Filters;
use App\Modules\Database\src\Models\FilterValues;
use App\Modules\Database\src\Models\Item;
use App\Modules\Database\src\Models\ItemCount;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductFilterValues;
use App\Modules\Database\src\Models\ProductProperties;
use App\Modules\Database\src\Models\Property;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use InvalidArgumentException;
use ShopManApi\Entity\OfferParams;
use ShopManApi\ShopManApi;
use ShopManApi\YMLParser\Offer\ShopMVendorOffer;

class ShopManImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sh:get';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Imports Offers from ShopManager.';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $shopId = (int)env('API_SHOPMANAGER_SHOP_ID');
        $key = env('API_SHOPMANAGER_KEY');
        $file = env('API_SHOPMANAGER_FILE');

        if (!$shopId || !$key || !$file) {
            throw new InvalidArgumentException('ShopManager credentials are required');
        }

        $this->info('Url: https://api.shopmanager.by/d/api/' . sprintf('%s?shopId=%d&key=%s', $file, $shopId, $key));

        $handler = (new ShopManApi($shopId, $key))->parseYml($file);
        $statistic = [
            'products' => 0,
            'items' => 0,
            'properties' => 0,
            'filters' => 0,
            'images' => 0,
        ];

        foreach ($handler->getOffers() as $i => $offer) {
            /** @var ShopMVendorOffer $offer */
            $product = $this->generateProductModel($offer);
            $item = $this->generateItemModel($offer, $product);
            ++$statistic['products'];
            ++$statistic['items'];

            // TODO: double check what is going on there
            // $itemCount = $this->generateItemCountModel($offer, $item);

            $statistic['images'] += $this->generateImages($offer, $product);
            $statistic['properties'] += $this->generateProperties($offer, $product);
            $statistic['filters'] += $this->generateFilters($offer, $product);

            if ($i > 15) break; // it's temporal
        }

        foreach ($statistic as $class => $count) {
            $this->info(sprintf(
                'Synchronized %d rows of %s-entity.',
                $count,
                $class
            ));
        }
    }

    private function generateProductModel(ShopMVendorOffer $offer): Product
    {
        $slug = 'prod'.$offer->getId(); // TODO: generate correct slug

        $product = Product::firstOrNew([
            'slug' => $slug, // is that fine?
        ]);
        $product->slug = $slug;
        $product->id = $product->id ?: $offer->getId();
        $product->title = $offer->getModel();
        $product->uuid = Str::uuid()->toString(); // is that fine for us?
        $product->price = $offer->getPrice();
        $product->visible = $offer->getAvailable();

        // TODO: fill correct descriptions
        $product->description = $offer->getDescription();
        $product->short_description = $offer->getDescription();
        $product->meta_description = '{}'; // is that json?
        $product->meta_tag = '{}'; // is that json?

        $product->save();

        return $product;
    }

    private function generateItemModel(ShopMVendorOffer $offer, Product $product): Item
    {
        $price = $offer->getPrice();
        $oldPrice = $offer->getOldprice();
        $discount = $oldPrice > $price ? 100* $oldPrice / $price : null;

        $productItem = Item::firstOrNew([
            'product_id' => $product->id,
        ]);
        $productItem->product_id = $product->id;
        $productItem->vendor_code = $offer->getVendor() ?: ($offer->getVendorCode() ?? '');
        $productItem->price = $price;
        $productItem->discount = $discount;
        $productItem->save();

        return $productItem;
    }

    private function generateItemCountModel(ShopMVendorOffer $offer, Item $productItem): ItemCount
    {
        $itemCount = ItemCount::firstOrCreate([
            'item_id' => $productItem->id,
        ]);
        $itemCount->count = $offer->getQuantityInStock();
        $itemCount->departure_point_id = 1; // TODO: use the real ID
        $itemCount->save();

        return $itemCount;
    }

    private function generateProperties(ShopMVendorOffer $offer, Product $product): int
    {
        $offerParams = new OfferParams($offer->getParams());

        $params = [
            'release_date' => $offerParams->getParamValue('Дата выхода на рынок') ?? '',

            'diameter_bike_frame' => $offerParams->getParamValue('Диаметр рамы велосипеда') ?? '',
            'bikes_number' => $offerParams->getParamValueInt('Количество велосипедов') ?? 0,
            'material' => $offerParams->getParamValue('Материал') ?? '',
            'installation_method' => $offerParams->getParamValue('Метод установки') ?? '',
            'weight_limit' => $offerParams->getParamValue('Ограничение веса') ?? '',
            'color' => $offerParams->getParamValue('Цвет') ?? '',
            'weight' => $offerParams->getParamValue('Вес') ?? '',

            'importer' => $offer->getImporter(),
            'service_center' => $offer->getServiceCenter(),

            // TODO: add more and for modula
        ];
        $params = array_filter($params);

        foreach ($params as $slug => $value) {
            $property = Property::firstOrCreate([
                'slug' => $slug,
            ]);

            $productProperty = ProductProperties::firstOrNew([
                'product_id' => $product->id,
                'property_id' => $property->id,
            ]);
            $productProperty->product_id = $product->id;
            $productProperty->property_id = $property->id;
            $productProperty->value = $value;
            $productProperty->save();
        }

        return count($params);
    }

    private function generateFilters(ShopMVendorOffer $offer, Product $product): int
    {
        $offerParams = new OfferParams($offer->getParams());

        $params = [
            'diameter_bike_frame' => $offerParams->getParamValue('Диаметр рамы велосипеда') ?? '',
            'bikes_number' => $offerParams->getParamValueInt('Количество велосипедов') ?? 0,
            'material' => $offerParams->getParamValue('Материал') ?? '',
            'installation_method' => $offerParams->getParamValue('Метод установки') ?? '',
            'weight_limit' => $offerParams->getParamValue('Ограничение веса') ?? '',
            'color' => $offerParams->getParamValue('Цвет') ?? '',
            'weight' => $offerParams->getParamValue('Вес') ?? '',
        ];
        $params = array_filter($params);

        foreach ($params as $slug => $value) {
            $filter = Filters::firstOrCreate([
                'slug' => $slug,
            ]);

            $filterValue = FilterValues::firstOrCreate([
                // TODO: generate the slug using latin letters only
                'slug' => sprintf('%s_%s', $slug, $value),
                'filter_id' => $filter->id,
            ]);

            $productFilterValue = ProductFilterValues::firstOrNew([
                'product_id' => $product->id,
                'filter_value_id' => $filterValue->id,
            ]);
            ///$productFilterValue->value = $value; // TODO: i guess we need that!
            $productFilterValue->save();
        }

        return count($params);
    }

    private function generateImages(ShopMVendorOffer $offer, Product $product): int
    {
        $pictures = $offer->getPictures() ?? [];

        foreach ($pictures as $picture) {
            // TODO:
        }

        return count($pictures);
    }
}
