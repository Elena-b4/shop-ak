<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckPhoneConfirmation
{
    public function handle(Request $request, Closure $next)
    {
        $user = \auth()->user();
        if (empty($user->phone_verified_at)) {
            abort(403, 'Your phone number is not confirmed.');
        }

        return $next($request);
    }
}
