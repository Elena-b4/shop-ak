<?php

use App\Modules\Api\src\Controllers\Auth\AuthController;
use App\Modules\Api\src\Controllers\Auth\ResetPasswordController;
use App\Modules\Api\src\Controllers\CartController;
use App\Modules\Api\src\Controllers\CategoryController;
use App\Modules\Api\src\Controllers\LikesController;
use App\Modules\Api\src\Controllers\MainInfoController;
use App\Modules\Api\src\Controllers\MenuController;
use App\Modules\Api\src\Controllers\Product\ProductController;
use App\Modules\Api\src\Controllers\Product\SearchController;
use App\Modules\Api\src\Controllers\ReviewController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


Route::group([

    'middleware' => ['api'],
    'prefix' => LaravelLocalization::setLocale() . '/api/v1/auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login'])->name('login');
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('register', [AuthController::class, 'register'])->middleware('throttle:5,60');
    Route::post('confirm-phone', [AuthController::class, 'confirmPhone'])->middleware('throttle:5,60');
});


Route::post('forget-password', [ResetPasswordController::class, 'submitForgetPasswordForm']);
Route::post('reset-password', [ResetPasswordController::class, 'submitResetPasswordForm']);

Route::post('refresh', [AuthController::class, 'refresh'])->middleware('jwt');
Route::post('send-code', [AuthController::class, 'sendCode'])->middleware('throttle:5,60');

Route::middleware('jwt.auth')->get('users', function () {
    return auth('api')->user();
});

Route::group(['prefix' => LaravelLocalization::setLocale() . '/api/v1/',
    'middleware' => ['jwt.auth', 'api', 'checkPhoneConfirmation']],
    function () {
        Route::get('me', [AuthController::class, 'me']);

        /* Cart */
        Route::post('/cart/add-items', [CartController::class, 'addItem']);
        Route::post('/cart/delete-items', [CartController::class, 'deleteItem']);
        Route::get('/cart', [CartController::class, 'index']);
        /* Cart */

        Route::post('/reviews/{reviewId}/mark', [ReviewController::class, 'addLikeOrDislike']);

    });


Route::group(['prefix' => LaravelLocalization::setLocale() . '/api/v1/',
    'middleware' => ['api']],
    function () {

        Route::get('/categories', [CategoryController::class, 'index']);
        Route::get('/categories/{slug}', [CategoryController::class, 'getCategoryBySlug']);
        Route::get('/categories/{category_slug}/products', [ProductController::class, 'getProductsInCatalog']);

        Route::get('/reviews/{productSlug}', [ReviewController::class, 'index']);
        Route::post('/reviews/{productSlug}', [ReviewController::class, 'sendReview']);

        Route::get('/menu', [MenuController::class, 'index']);

        Route::get('/categories/{category_slug}/products/{slug}', [ProductController::class, 'index']);

        Route::get('/main-info', [MainInfoController::class, 'index']);

        Route::get('/search', [SearchController::class, 'search']);
        Route::get('/search-page', [SearchController::class, 'searchPage']);

        // likes
        Route::post('/like', [LikesController::class, 'setUnsetLike']);
        Route::post('/sync', [LikesController::class, 'sync']);



    });
