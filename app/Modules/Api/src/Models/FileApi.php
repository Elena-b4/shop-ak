<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Common\src\Helpers\S3;
use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\File;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Storage;

class FileApi extends File
{

    protected $appends = ['url'];

    protected $table = FileEnum::TABLE_NAME;

    protected $fillable = [FileEnum::NAME, MainEnum::SORT, MainEnum::VISIBLE, FileEnum::TYPE, FileEnum::ENTITY, FileEnum::ENTITY_ID];

    protected $visible = ['name'];

    public function getNameAttribute($value)
    {
        return S3::getUrl($value);
    }
}
