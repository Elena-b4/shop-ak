<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ReviewEnum;
use App\Modules\Database\src\Enums\ReviewLikeEnum;
use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Eloquent\Model;

class ReviewLikeApi extends Model
{
    protected $table = ReviewLikeEnum::TABLE_NAME;

    protected $hidden = ['id', 'created_at', 'updated_at', ProductEnum::PRODUCT_ID, ReviewEnum::REVIEW_ID, UserEnum::USER_ID];

    public function review()
    {
        return $this->belongsTo(ReviewApi::class, 'id', ReviewEnum::REVIEW_ID);
    }
}
