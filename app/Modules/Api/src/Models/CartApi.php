<?php

namespace App\Modules\Api\src\Models;


use App\Modules\Database\src\Models\Cart;

class CartApi extends Cart
{

    protected $hidden = ['created_at', 'updated_at'];

}
