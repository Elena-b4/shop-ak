<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ProductPropertyEnum;
use App\Modules\Database\src\Enums\PropertyEnum;
use App\Modules\Database\src\Enums\UserEnum;
use App\Modules\Database\src\Models\Brand;
use App\Modules\Database\src\Models\Filter;
use App\Modules\Database\src\Models\FilterValue;
use App\Modules\Database\src\Models\Like;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductNames;
use App\Modules\Database\src\Models\Property;
use Illuminate\Support\Facades\Auth;
use Laravel\Scout\Searchable;

class ProductApi extends Product
{

    /**
     * @property int $id
     */

    use Searchable;

    protected $hidden = [MainEnum::VISIBLE, 'created_at', 'updated_at',
        MainEnum::TITLE, MainEnum::SORT, 'names', ProductEnum::BRAND_ID,
        ProductEnum::META_DESCRIPTION, ProductEnum::META_TAG, 'brands'];
    protected $appends = [ProductEnum::NAME, 'brand', 'main_image', 'like'];
    private $lang;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->lang = app()->getLocale();
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
        ];
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'product_filters', ProductEnum::PRODUCT_ID, FilterEnum::FILTER_ID);
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class, ProductPropertyEnum::TABLE_NAME, ProductEnum::PRODUCT_ID, PropertyEnum::PROPERTY_ID);
    }

    public function brands()
    {
        return $this->hasOne(Brand::class, 'id', ProductEnum::BRAND_ID);
    }

    public function items()
    {
        return $this->hasMany(ItemApi::class, ProductEnum::PRODUCT_ID, 'id');
    }

    public function values()
    {
        return $this->belongsToMany(FilterValue::class, PivotTableEnum::PRODUCT_FILTER_VALUES, ProductEnum::PRODUCT_ID, FilterValueEnum::FILTER_VALUE_ID);
    }


    public function names()
    {
        return $this->hasMany(ProductNames::class, ProductEnum::PRODUCT_ID, 'id');
    }

    public function images()
    {
        return $this->hasMany(FileApi::class, FileEnum::ENTITY_ID, 'id')->where('entity', ProductEnum::TABLE_NAME);
    }

    public function getMainImageAttribute()
    {
        return $this->images->first()->name ?? null;
    }

    public function video()
    {
        return $this->hasMany(FileApi::class, FileEnum::ENTITY_ID, 'id')->where('entity', ProductEnum::TABLE_NAME)->where(FileEnum::TYPE, 'video');
    }


    public function getDescriptionAttribute($value)
    {
        return json_decode($value)->{ProductEnum::DESCRIPTION . $this->lang} ?? null;
    }

    public function getLikeAttribute($value)
    {
        if (Auth::check() &&
            Like::where([ProductEnum::PRODUCT_ID => $this->id, UserEnum::USER_ID => Auth::id()])->exists()) {
            return 1;
        }
        return 0;
    }

    public function getMetaDescriptionAttribute($value)
    {
        return json_decode($value)->{ProductEnum::META_DESCRIPTION . $this->lang} ?? null;
    }

    public function getShortDescriptionAttribute($value)
    {
        return json_decode($value)->{ProductEnum::SHORT_DESCRIPTION . $this->lang} ?? null;
    }

    public function getTitleAttribute($value)
    {
        return json_decode($value)->{MainEnum::TITLE . $this->lang} ?? null;
    }

    public function getMetaTagAttribute($value)
    {
        return json_decode($value)->{ProductEnum::META_TAG . $this->lang} ?? null;
    }


    public function getNameAttribute()
    {
        return $this->names->keyBy('lang')->toArray()[$this->lang][ProductEnum::NAME];
    }

    public function getBrandAttribute()
    {
        try {
            return $this->brands->name;
        } catch (\Exception $exception) {
            return null;
        }
    }

}
