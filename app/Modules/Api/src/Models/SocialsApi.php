<?php


namespace App\Modules\Api\src\Models;


use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\SocialEnum;
use App\Modules\Database\src\Models\Socials;

class SocialsApi extends Socials
{

    private $lang;

    protected $hidden = ['id', MainEnum::SORT, 'created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->lang = app()->getLocale();
    }

    public function getNameAttribute($value)
    {
        return json_decode($value)->{SocialEnum::NAME . $this->lang};
    }
}
