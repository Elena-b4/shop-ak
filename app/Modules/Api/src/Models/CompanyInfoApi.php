<?php


namespace App\Modules\Api\src\Models;


use App\Modules\Database\src\Enums\CompanyInfoEnum;
use App\Modules\Database\src\Enums\SocialEnum;
use App\Modules\Database\src\Models\CompanyInfo;

class CompanyInfoApi extends CompanyInfo
{

    private $lang;

    protected $hidden = ['id', 'created_at', 'updated_at'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->lang = app()->getLocale();
    }

    public function getPhonesAttribute($phones)
    {
        $phones = $phones ? explode(';', $phones) : [];
        foreach ($phones as &$phone) {
            $phone = trim($phone);
        }
        return $phones;
    }

    public function getAddressAttribute($value)
    {
        return json_decode($value)->{CompanyInfoEnum::ADDRESS . $this->lang};
    }

    public function getDescriptionAttribute($value)
    {
        return json_decode($value)->{CompanyInfoEnum::DESCRIPTION . $this->lang};
    }

    public function getNameAttribute($value)
    {
        return json_decode($value)->{CompanyInfoEnum::NAME . $this->lang};
    }

    public function getMetaDescriptionAttribute($value)
    {
        return json_decode($value)->{CompanyInfoEnum::META_DESCRIPTION . $this->lang};
    }

    public function getMetaTagAttribute($value)
    {
        return json_decode($value)->{CompanyInfoEnum::META_TAG . $this->lang};
    }

}
