<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ReviewEnum;
use App\Modules\Database\src\Models\Product;
use Illuminate\Database\Eloquent\Model;

class ReviewApi extends Model
{
    protected $table = ReviewEnum::TABLE_NAME;

    protected $hidden = ['id', 'created_at', 'updated_at', ReviewEnum::PRODUCT_UUID, MainEnum::PARENT_ID];

    public function children()
    {
        return $this->hasMany(ReviewApi::class, MainEnum::PARENT_ID)->with(['children', 'likes']);
    }

    public function likes()
    {
        return $this->hasMany(ReviewLikeApi::class, ReviewEnum::REVIEW_ID, 'id');
    }

}
