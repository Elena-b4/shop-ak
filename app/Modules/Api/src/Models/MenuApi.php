<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use App\Modules\Database\src\Models\Menu;

class MenuApi extends Menu
{
    protected $hidden = [MainEnum::SLUG, MainEnum::SORT, 'created_at', 'updated_at'];

    public function items()
    {
        return $this->hasMany(MenuItemsApi::class, MenuEnum::MENU_ID);
    }

}
