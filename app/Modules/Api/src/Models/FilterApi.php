<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Filter;

class FilterApi extends Filter
{

    protected $appends = ['key', 'title'];

    protected $hidden = [MainEnum::SLUG, MainEnum::SORT, MainEnum::VISIBLE, 'created_at', 'updated_at', 'pivot'];

    public function values(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(FiltersValuesApi::class, FilterEnum::FILTER_ID, 'id');
    }

    public function getKeyAttribute()
    {
        return 'values';
    }
}
