<?php

namespace App\Modules\Api\src\Models;


use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Category;

class CategoryApi extends Category
{

    protected $hidden = [MainEnum::SORT, MainEnum::VISIBLE, MainEnum::PARENT_ID, 'created_at', 'updated_at'];


    public function children()
    {
        return $this->hasMany(CategoryApi::class, MainEnum::PARENT_ID)->with('children');
    }

    public function getTitleAttribute()
    {
        return __('category.' . $this->attributes[MainEnum::SLUG]);
    }


    public function filters()
    {
        return $this->belongsToMany(FilterApi::class, 'category_filters', CategoryEnum::CATEGORY_ID, FilterEnum::FILTER_ID);
    }


}
