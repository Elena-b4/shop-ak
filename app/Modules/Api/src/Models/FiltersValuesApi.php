<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\FilterValue;

class FiltersValuesApi extends FilterValue
{
    protected $hidden = [FilterEnum::FILTER_ID, MainEnum::SLUG, 'created_at', 'updated_at', MainEnum::SORT];

    public function filter()
    {
        return $this->belongsTo(FilterApi::class, 'id', FilterEnum::FILTER_ID);
    }

}
