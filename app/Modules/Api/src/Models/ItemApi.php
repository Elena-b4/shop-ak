<?php

namespace App\Modules\Api\src\Models;

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ShopOrderEnum;
use App\Modules\Database\src\Models\Item;
use App\Modules\Database\src\Models\ShopOrder;

class ItemApi extends Item
{
    protected $table = ItemEnum::TABLE_NAME;

    protected $hidden = ['id', 'created_at', 'updated_at', ProductEnum::PRODUCT_ID];

    public function images()
    {
        return $this->hasMany(FileApi::class, FileEnum::ENTITY_ID, 'id')->where('entity', ItemEnum::TABLE_NAME);
    }

    public function product()
    {
        return $this->hasOne(ProductApi::class, 'id', ProductEnum::PRODUCT_ID);
    }

    public function shopOrder()
    {
        return $this->belongsToMany(ShopOrder::class, PivotTableEnum::ITEM_SHOP_ORDER, ItemEnum::ITEM_ID, ShopOrderEnum::SHOP_ORDER_ID);
    }

}
