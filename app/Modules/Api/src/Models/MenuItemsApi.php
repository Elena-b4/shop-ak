<?php

namespace App\Modules\Api\src\Models;


use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use App\Modules\Database\src\Models\MenuItems;

class MenuItemsApi extends MenuItems
{
    protected $hidden = [MainEnum::SLUG, MainEnum::SORT, MainEnum::PARENT_ID, MainEnum::VISIBLE,
        MenuEnum::MENU_ID, 'created_at', 'updated_at',];

    public function children()
    {
        return $this->hasMany(MenuItemsApi::class, MainEnum::PARENT_ID)->with('children');
    }


}
