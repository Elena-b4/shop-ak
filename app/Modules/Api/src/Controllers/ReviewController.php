<?php

namespace App\Modules\Api\src\Controllers;

use App\Modules\Api\src\Repositories\Contracts\ProductRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\ReviewRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ReviewController extends BaseController
{

    private $reviewRepository;
    private $productRepository;

    public function __construct(ReviewRepositoryInterface $reviewRepository,
                                ProductRepositoryInterface $productRepository)
    {
        $this->reviewRepository = $reviewRepository;
        $this->productRepository = $productRepository;
    }

    public function index($productSlug): JsonResponse
    {
        $product = $this->productRepository->getProductBySlug($productSlug);

        if ($product) {
            $reviews = $this->reviewRepository->getReviewList($product->uuid);
            return $this->sendResponse($reviews);
        }

        return $this->sendError('Product doesn\'t exist.', 404);
    }

    public function sendReview(Request $request, string $productSlug): JsonResponse
    {
        $product = $this->productRepository->getProductBySlug($productSlug);

        if (!$product) {
            return $this->sendError('Product doesn\'t exist.', 404);
        }
        $data = $request->all();
        $review = $this->reviewRepository->sendReview($product->uuid, $data);
        if (!$review) {
            return $this->sendError('Review doesn\'t saved.', 404);
        }
        return $this->sendResponse($review);
    }

    public function addLikeOrDislike(Request $request, $reviewId)
    {
        $review = $this->reviewRepository->getReviewById($reviewId);
        $data = $request->all();
        if ($review && isset($data['value'])) {
            $reviewLike = $this->reviewRepository->addLikeOrDislike($review, $data);
            if (!$reviewLike) {
                return $this->sendError('Invalid data.', 404);
            }
            return $this->sendResponse('success', 200);
        }
        return $this->sendError('Invalid data.', 404);
    }
}
