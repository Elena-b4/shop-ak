<?php

namespace App\Modules\Api\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\UserEnum;
use App\Modules\Database\src\Models\Like;
use Illuminate\Support\Facades\Auth;

class LikesController extends Controller
{

    public function setUnsetLike($productId)
    {
        $like = Like::where([ProductEnum::PRODUCT_ID => $productId, UserEnum::USER_ID => Auth::id()]);
        if ($like->exists()) {
            $like->delete();
        } else {
            Like::create([ProductEnum::PRODUCT_ID => $productId, UserEnum::USER_ID => Auth::id()]);
        }
    }

    public function sync($productIds)
    {
        $userLikedProductIds = Like::where([ UserEnum::USER_ID => Auth::id()])->pluck('product_id')->toArray();

        $diff = array_diff($productIds, $userLikedProductIds);

        $data = [];
        foreach ($diff as $productId) {
            $data[] = [ UserEnum::USER_ID => Auth::id(), ProductEnum::PRODUCT_ID => $productId];
        }

        Like::create($data);
    }
}
