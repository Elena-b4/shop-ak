<?php

namespace App\Modules\Api\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Api\src\Models\MenuApi;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use function response;

class MenuController extends Controller
{

    public function index()
    {

        $menu = MenuApi::with(['items' => function ($query) {
            $query->orderBy(MainEnum::SORT);
            $query->where('menu_items.parent_id', 0);
            $query->with(['children' => function ($query) {
                $query->orderBy(MainEnum::SORT);
            }]);
        }])
            ->where(MainEnum::VISIBLE, true)
            ->orderBy(MainEnum::SORT)
            ->select(MainEnum::SLUG, 'id', MenuEnum::URL)
            ->get([MainEnum::SLUG, 'id', MainEnum::PARENT_ID, MenuEnum::URL])
            ->toArray();

        return response()->json($menu);

    }

}
