<?php

namespace App\Modules\Api\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Api\src\Repositories\CartRepository;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use function response;

class CartController extends Controller
{

    private $cartRepository;

    public function __construct(CartRepository $cartRepository)
    {
        $this->cartRepository = $cartRepository;
    }

    public function index(): JsonResponse
    {
        $cartItems = $this->cartRepository->getList();
        return response()->json($cartItems);
    }

    public function addItem(Request $request): JsonResponse
    {
        if (
            !$request->item_id
            || !$this->cartRepository->addItemToCart($request->item_id, $request->count)
        ) {
            return response()->json(['message' => 'Не удалось добавить товар в корзину.'], 403);
        }

        return response()->json();
    }

    public function deleteItem(Request $request): JsonResponse
    {
        if (
            !$request->vendor_code
            || !$this->cartRepository->deleteItemFromCart($request->vendor_code, $request->type)
        ) {
            return response()->json(['message' => 'Не удалось удалить товар в корзину.'], 403);
        }

        return response()->json();
    }

}
