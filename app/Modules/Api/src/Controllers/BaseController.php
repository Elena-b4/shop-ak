<?php

namespace App\Modules\Api\src\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use function response;

abstract class BaseController extends Controller
{


    /**
     * success response method.
     *
     * @param null $data
     * @param int $code
     * @return JsonResponse
     */
    protected function sendResponse($data = null, $code = 200)
    {
        return response()->json(['data' => $data, 'code' => $code], $code);
    }

    /**
     * return error response.
     *
     * @param $message
     * @param int $code
     * @param array $errors
     * @return JsonResponse
     */
    protected function sendError($message = 'Not found', $code = 404, $errors = null)
    {
        return response()->json(['message' => $message, 'errors' => $errors, 'code' => $code], $code);
    }

    protected function paginate($items, $perPage = 15, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }


}
