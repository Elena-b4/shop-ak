<?php

namespace App\Modules\Api\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Api\src\Repositories\CategoryRepository;
use App\Modules\Api\src\Repositories\Contracts\CategoryRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\ProductRepositoryInterface;
use App\Modules\Api\src\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use function response;

class CategoryController extends Controller
{

    private CategoryRepository $categoryRepository;
    private ProductRepository $productRepository;

    public function __construct(CategoryRepositoryInterface $categoryRepository,
                                ProductRepositoryInterface  $productRepository)
    {
        /** @var ProductRepository $productRepository */
        $this->productRepository = $productRepository;

        /** @var CategoryRepository $categoryRepository */
        $this->categoryRepository = $categoryRepository;


    }

    public function index(): JsonResponse
    {
        $categories = $this->categoryRepository->getCategoryList();
        return response()->json($categories);
    }

    public function getCategoryBySlug(string $slug): JsonResponse
    {
        $category = $this->categoryRepository->getCategoryBySlug($slug);
        if (!$category) {
            abort(404);
        }
        $categoryChildren = $this->categoryRepository->getAllChildren($slug);
        $productIds = $this->productRepository->getProductIdsIdsInCategory($categoryChildren);
        $productValuesIdsInCategory = $this->productRepository->getProductFilterValuesIdsInCategory($productIds);
        $minMaxPrice = $this->productRepository->getMinMaxPriceInCategory($productValuesIdsInCategory);
        $brands = $this->productRepository->getBrands($productIds);
        return response()->json(['category' => $category, 'values' => $productValuesIdsInCategory, 'price' => $minMaxPrice, 'brands' => $brands]);
    }
}
