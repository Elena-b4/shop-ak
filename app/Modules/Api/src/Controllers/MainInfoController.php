<?php


namespace App\Modules\Api\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Api\src\Repositories\CompanyInfoRepository;
use App\Modules\Api\src\Repositories\Contracts\CategoryRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\ProductRepositoryInterface;
use App\Modules\Api\src\Repositories\SocialRepository;
use Illuminate\Http\JsonResponse;

class MainInfoController extends Controller
{

    private SocialRepository $socialRepository;
    private CompanyInfoRepository $companyInfoRepository;

    public function __construct(SocialRepository $socialRepository, CompanyInfoRepository $companyInfoRepository)
    {
        $this->socialRepository = $socialRepository;
        $this->companyInfoRepository = $companyInfoRepository;
    }

    public function index(): JsonResponse
    {
        $socials = $this->socialRepository->getSocials();
        $companyInfo = $this->companyInfoRepository->getCompanyInfo();
        return response()->json(['socials' => $socials, 'companyInfo' => $companyInfo]);
    }


}
