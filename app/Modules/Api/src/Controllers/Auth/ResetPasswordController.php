<?php


namespace App\Modules\Api\src\Controllers\Auth;


use App\Http\Controllers\Controller;
use App\Modules\Api\src\Requests\Auth\ForgetPasswordRequest;
use App\Modules\Api\src\Requests\Auth\ResetPasswordRequest;
use App\Modules\Common\src\Helpers\Mail as MailHelper;
use App\Modules\Common\src\services\MailService;
use App\Modules\Database\src\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    private MailService $mailService;

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    public function submitForgetPasswordForm(ForgetPasswordRequest $request)
    {
        $token = Str::random(64);

        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $this->mailService->sendTokenForResetPassword(MailHelper::RESET_PASSWORD_SUBJECT, $request->email,
            ['url' => config('app.url') . '/form-reset-password/' . $token]);
        return response('Token was sent successfully!');
    }

    public function submitResetPasswordForm(ResetPasswordRequest $request)
    {
        $updatePassword = DB::table('password_resets')
            ->where([
                'email' => $request->email,
                'token' => $request->token
            ])
            ->first();

        if(!$updatePassword){
            return response('Invalid token!', 403);
        }

        User::where('email', $request->email)
            ->update(['password' => Hash::make($request->password)]);

        DB::table('password_resets')->where(['email'=> $request->email])->delete();

        return response('Your password has been changed!', 200);
    }

}
