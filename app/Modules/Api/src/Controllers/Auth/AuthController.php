<?php

namespace App\Modules\Api\src\Controllers\Auth;

use App\Modules\Api\src\Controllers\BaseController;
use App\Modules\Api\src\Services\Contracts\SmsService;
use App\Modules\Api\src\Services\PhoneSmsService;
use App\Modules\Api\src\Services\TelegramSmsService;
use App\Modules\Api\src\Requests\Auth\Login;
use App\Modules\Api\src\Requests\Auth\UserSignUp;
use App\Modules\Database\src\Enums\RequestMessages;
use App\Modules\Database\src\Models\SmsCode;
use App\Modules\Database\src\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Request;

class AuthController extends BaseController
{
    use AuthenticatesUsers;

    public SmsService $smsService;

    public function __construct()
    {
        match (config('app.env')) {
            'local' => app()->bind(SmsService::class, TelegramSmsService::class),
            'production' => app()->bind(SmsService::class, PhoneSmsService::class)
        };
        $this->smsService = app()->make(SmsService::class);
        $this->middleware('auth:api', ['except' => ['login', 'register', 'me', 'refresh']]);
    }

    public function login(Login $request)
    {
        $this->username();
        $credentials = request(['phone', 'password']);
        if ($token = auth()->attempt($credentials)) {
            return $this->respondWithToken($token);
        } else {
            if (User::where('phone', $request->username)->exists()) {
                return $this->sendError('The given data was invalid', 422, ['password' => RequestMessages::INCORRECT_PASSWORD]);
            }

            return $this->sendError('The given data was invalid', 422, ['phone' => RequestMessages::USER_NOT_EXISTS]);
        }
    }

    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function changePassword(ChangePassword $request)
    {
        $user = $request->user();
        if (password_verify($request->password, $user->password)) {
            User::find($user->id)->update(['password' => $request->new_password]);
            return response()->json(['success' => true], 201);
        } else {
            return $this->sendError(RequestMessages::INCORRECT_PASSWORD);
        }
    }

    public function register(UserSignUp $request)
    {
        $data = $request->all();
        $user = new User();
        $user->name = $data['name'];
        $user->password = Hash::make($data['password']);
        $user->phone = $data['phone'];
        $user->save();

        $code = SmsService::saveCode($user->phone);
        $this->smsService->sendSms($user->name, $code);
        $token = auth('api')->attempt(['phone' => $request->phone, 'password' => $request->password]);

        return $this->respondWithToken($token);
    }

    public function confirmPhone(Request $request): void
    {
        $code = SmsCode::where(['phone' => $request->phone])->latest()->first();
        if (!Hash::check($request->code, $code)) {
            abort(422, 'Invalid code.');
        }
        $user = User::where(['phone' => $request->phone])->first();
        $user->phone_verified_at = Carbon::now();
    }

    public function sendCode(Request $request): int
    {
        return SmsService::saveCode($request->phone);
    }

    public function me()
    {
        dd(1231);
    }

    private function username()
    {
        $login = request()->input('username');

        $phone = str_replace(['(', ')', '-', '+', ' '], '', trim($login));
        \request()->merge([
            'phone' => $phone,
        ]);
    }

    private function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
