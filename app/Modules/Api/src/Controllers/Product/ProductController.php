<?php

namespace App\Modules\Api\src\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Modules\Api\src\Repositories\Contracts\ProductRepositoryInterface;
use App\Modules\Api\src\Repositories\ProductRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use function response;

class ProductController extends Controller
{

    private ProductRepository $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        /** @var ProductRepository $productRepository */
        $this->productRepository = $productRepository;
    }

    public function index(string $category_slug, string $slug): JsonResponse
    {
        return response()->json($this->productRepository->getProductBySlug($slug));
    }

    public function getProductsInCatalog(Request $request, $slug): JsonResponse
    {
        $product = $this->productRepository->getProducts($request, $slug);
        return response()->json($product);

    }


}
