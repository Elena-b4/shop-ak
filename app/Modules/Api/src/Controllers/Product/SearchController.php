<?php

namespace App\Modules\Api\src\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Modules\Api\src\Models\ProductApi;
use App\Modules\Api\src\Repositories\Contracts\ProductRepositoryInterface;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductNames;
use Illuminate\Http\Request;

class SearchController extends Controller
{

    private $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function search(Request $request)
    {
//        $productIdsFromNames = array_keys(ProductNames::search($request->search)->get()->keyBy(ProductEnum::PRODUCT_ID)->toArray());
        $productIdsFromNames = [];
        $productIdsFromProducts = array_keys(Product::search($request->search)->get()->keyBy('id')->toArray());
        $productIds = array_unique(array_merge($productIdsFromNames, $productIdsFromProducts));
        $products = ProductApi::with('images')->whereIn('id', $productIds)->select('id', MainEnum::SLUG, ProductEnum::PRICE)->take(10)->get()->makeHidden(['names', 'images'])->toArray();
        return $products;

    }

    public function searchPage(Request $request)
    {
        $productIdsFromNames = array_keys(ProductNames::search($request->search)->get()->keyBy(ProductEnum::PRODUCT_ID)->toArray());
        $productIdsFromProducts = array_keys(ProductApi::search($request->search)->get()->keyBy('id')->toArray());
        $productIds = array_unique(array_merge($productIdsFromNames, $productIdsFromProducts));
        return $this->productRepository->getProductsForSearch($productIds);
        return $products;

    }
}
