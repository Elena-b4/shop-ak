<?php

namespace App\Modules\Api\src\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class UserSignUp extends FormRequest
{

    protected function prepareForValidation()
    {
        if ($this->has('phone'))
            $phone = str_replace(['(', ')', '-', '+', ' '], '', trim($this->phone));
        $this->merge(['phone' => $phone]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|unique:users',
            'name' => 'required',
            'password' => 'required|string|min:8',
            'confirm_password' => 'required|string|same:password'
        ];
    }

    public function messages()
    {
        $message['phone.unique'] = __('validation.phone_is_exist');
        return $message;
    }
}
