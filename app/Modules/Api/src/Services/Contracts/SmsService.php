<?php

namespace App\Modules\Api\src\Services\Contracts;

use App\Modules\Database\src\Models\SmsCode;
use Illuminate\Support\Facades\Hash;

abstract class SmsService
{
    public abstract function sendSms(string $userName, int $userCode);

    public static function saveCode($phone): int
    {
        $randomCode = random_int(1000, 9999);

        $code = new SmsCode();
        $code->code = Hash::make($randomCode);
        $code->phone = $phone;
        $code->save();

        return $randomCode;
    }
}
