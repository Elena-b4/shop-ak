<?php
declare(strict_types=1);


namespace App\Modules\Api\src\Services;

use App\Modules\Api\src\Services\Contracts\SmsService;
use Telegram\Bot\Api;

class TelegramSmsService extends SmsService
{
    public function sendSms(string $userName, int $userCode)
    {
        $telegram = new Api(config('telegram.bots.foxy.token'));

        $chatId = (int)env('MY_CHAT_ID');
        if ($chatId != null) {
            $telegram->sendMessage([
                'chat_id' => $chatId,
                'text' => 'Привет, ' . $userName . '. Это Foxy! Твой код для подтверждения: ' . $userCode]);
        }
    }

}
