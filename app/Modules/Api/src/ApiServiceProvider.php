<?php


namespace App\Modules\Api\src;


use App\Modules\Api\src\Repositories\CartRepository;
use App\Modules\Api\src\Repositories\CategoryRepository;
use App\Modules\Api\src\Repositories\Contracts\CartRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\CategoryRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\ProductRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\ReviewRepositoryInterface;
use App\Modules\Api\src\Repositories\Contracts\SocialRepositoryInterface;
use App\Modules\Api\src\Repositories\ProductRepository;
use App\Modules\Api\src\Repositories\ReviewRepository;
use App\Modules\Api\src\Repositories\SocialRepository;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class ApiServiceProvider extends ServiceProvider
{

    public $bindings = [
        CategoryRepositoryInterface::class => CategoryRepository::class,
        ProductRepositoryInterface::class => ProductRepository::class,
        SocialRepositoryInterface::class => SocialRepository::class,
        CartRepositoryInterface::class => CartRepository::class,
        ReviewRepositoryInterface::class => ReviewRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Route::middleware([])->group(__DIR__ . '/../routes/routes.php');
    }


}
