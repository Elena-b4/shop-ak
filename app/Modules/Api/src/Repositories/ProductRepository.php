<?php

namespace App\Modules\Api\src\Repositories;

use App\Modules\Api\src\Models\ProductApi;
use App\Modules\Api\src\Repositories\Contracts\CategoryRepositoryInterface;
use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\Brand;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductCategories;
use App\Modules\Database\src\Models\ProductFilterValue;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProductRepository implements Contracts\ProductRepositoryInterface
{

    public function getProductBySlug($slug)
    {
        try {
            $productUud = Product::where(MainEnum::SLUG, $slug)->select([ProductEnum::UUID])->first()->uuid;
        } catch (\Exception $exception) {
            abort(404);
        }

        return ProductApi::where(ProductEnum::UUID, $productUud)
            ->with(['images' => function ($query) {
                $query->orderBy(MainEnum::SORT);
                $query->where(FileEnum::TYPE, 'img');
            }])
            ->whereHas('items')
            ->with(['items.images'])
            ->with(['filters.values', 'values'])
            ->get();
    }


    public function getProducts(Request $request, string $slug): ?array
    {

        $products = ProductApi::where(MainEnum::VISIBLE, 1)->with(['images' => function ($query) {
            $query->orderBy(MainEnum::SORT);
            $query->where(FileEnum::TYPE, 'img');
        }])
            ->whereHas('items', function ($query) {
                $query->where(ItemEnum::QUANTITY, '>', 0);
            });

        $this->setCategory($products, $slug);

        if (isset($request->values)) {
            $values = explode(',', $request->values);
            foreach ($values as $value) {
                $products->whereHas('values', function ($query) use ($value) {
                    $query->where([FilterValueEnum::FILTER_VALUE_ID => $value]);
                });
            }
        }

        if (isset($request->brand)) {
            $brands = explode(',', $request->brand);
            $products->whereIn(ProductEnum::BRAND_ID, $brands);
        }

        $this->sortByParams($products, $request);
        $this->sortByPrice($products, $request);

        $productsClass = clone $products;

        $productIds = $productsClass->pluck('id');

        $product = $products->paginate(9);

        $productValues = $this->getProductFilterValuesIdsInCategory($productIds->toArray());

        if (!$product) {
            return null;
        }

        return ['products' => $product->toArray(), 'values' => $productValues];
    }

    public function setCategory(Builder $product, string $slug)
    {

        $product->whereHas('categories', function ($query) use ($slug) {
            $categoryRepository = app()->make(CategoryRepositoryInterface::class);
            $query->whereIn(CategoryEnum::CATEGORY_ID, $categoryRepository->getAllChildren($slug));
        });
    }

    public function sortByParams(Builder $productApi, Request $request)
    {
        if(! isset($request->order_type) || $request->order_type === null) {
            $orderType = 'asc';
        } else {
            $orderType = $request->order_type;
        }
        switch ($request->sort) {
            case 'popular' :
                $productApi->orderBy(ProductEnum::POPULAR, $orderType);
                break;
            case 'price' :
                $productApi->orderBy(ProductEnum::PRICE, $orderType);
                break;
            case 'new' :
                $productApi->orderBy('created_at', $orderType);
                break;
            default:
                $productApi->orderBy(MainEnum::SORT);
        }
    }

    public function sortByPrice(Builder $productApi, Request $request): void
    {
        if ($request->price) {
            $range = explode(',', $request->price);
            if (count($range) === 2)
                $productApi->whereBetween(ProductEnum::PRICE, [$range[0], $range[1]]);
        }

    }

    public function getProductFilterValuesIdsInCategory(array $productIds): array
    {
        $values = ProductFilterValue::whereIn(ProductEnum::PRODUCT_ID, $productIds)
            ->whereHas('filters', function ($query) {
                $query->where(MainEnum::VISIBLE, 1);
            })
            ->groupBy('filter_value_id')
            ->selectRaw('filter_value_id, count(product_id)')->get()->keyBy('filter_value_id')->toArray();

        $result = [];
        foreach ($values as $key => $value) {
            $result[$key] = $value['count'];
        }
        return $result;

    }

    public function getProductsForSearch($productIds)
    {
        return ProductApi::whereIn('id', $productIds)->where(MainEnum::VISIBLE, 1)->with(['images' => function ($query) {
            $query->orderBy(MainEnum::SORT);
            $query->where(FileEnum::TYPE, 'img');
        }])
            ->whereHas('items')->paginate();
    }

    public function getProductIdsIdsInCategory(array $categoryChildren): array
    {
        return ProductCategories::whereIn(CategoryEnum::CATEGORY_ID, $categoryChildren)->get(['id'])->toArray();
    }

    public function getMinMaxPriceInCategory(array $productIds): array
    {
        $productMinPrice = Product::whereIn('id', $productIds)->select(DB::raw('MIN(price) as min_price, Max(price) as max_price'))
            ->get(['min_price', 'max_price'])->toArray();

        $result['minPrice'] = $productMinPrice[0]['min_price'];
        $result['maxPrice'] = $productMinPrice[0]['max_price'];
        return $result;
    }

    public function getBrands($productIds)
    {
        $brandIds = array_unique(Product::whereIn('id', $productIds)->pluck(ProductEnum::BRAND_ID)->toArray());
        $brands = Brand::whereIn('id', $brandIds)->get();
        $brands = $brands->map(function ($brand) {
            return [
                'id' => $brand->id,
                'title' => $brand->name,
            ];
        });
        $brand['key'] = 'brand';
        $brand['title'] = __('table.brand');
        $brand['values'] = $brands;
        return $brand;
    }

}
