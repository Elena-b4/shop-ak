<?php

namespace App\Modules\Api\src\Repositories;

use App\Modules\Api\src\Models\CartApi;
use App\Modules\Api\src\Models\ItemApi;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CartRepository implements Contracts\CartRepositoryInterface
{

    public function getList(): array
    {
        return CartApi::where(UserEnum::USER_ID, Auth::id())->get()->toArray();
    }

    public function addItemToCart($itemId, $count): bool
    {
        if (!$item = ItemApi::where('id', $itemId)->get()->first()) {
            return false;
        }

        $userId = Auth::id();
        if ($cartItem = CartApi::where(ItemEnum::ITEM_ID, $item->id)->where(UserEnum::USER_ID, $userId)->get()->first()) {
            $cartItem->count += $count ?: 1;
            $result = $cartItem->save();
        } else {
            $newItem = new CartApi();
            $newItem->user_id = $userId;
            $newItem->item_id = $item->id;
            $newItem->count = $count ?: 1;
            $result = $newItem->save();
        }

        return $result;
    }

    public function deleteItemFromCart($vendorCode, $type): bool
    {
        if (
            !($item = ItemApi::where('vendor_code', $vendorCode)->get()->first())
            || !($cartItem = CartApi::where(UserEnum::USER_ID, Auth::id())->where(ItemEnum::ITEM_ID, $item->id)->first())
        ) {
            return false;
        }

        if (!$cartItem->count || $cartItem->count == 1 || $type == 'all') {
            $result = $cartItem->delete();
        } else {
            $cartItem->count -= 1;
            $result = $cartItem->save();
        }

        return $result;
    }

}
