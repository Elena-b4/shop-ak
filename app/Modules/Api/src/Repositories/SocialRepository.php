<?php

namespace App\Modules\Api\src\Repositories;

use App\Modules\Api\src\Models\SocialsApi;
use App\Modules\Database\src\Enums\MainEnum;

class SocialRepository implements Contracts\SocialRepositoryInterface
{

    public function getSocials(): array
    {
        return SocialsApi::orderBy(MainEnum::SORT)->get()->toArray();
    }

}
