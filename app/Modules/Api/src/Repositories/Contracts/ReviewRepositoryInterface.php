<?php

namespace App\Modules\Api\src\Repositories\Contracts;

use Illuminate\Http\Request;

interface ReviewRepositoryInterface
{
    public function addLikeOrDislike($review, $data);

    public function getReviewById($reviewId);

    public function getReviewList($productUUID);

    public function sendReview($productUUID, $data);

}
