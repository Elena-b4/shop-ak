<?php


namespace App\Modules\Api\src\Repositories\Contracts;


interface CompanyInfoRepositoryInterface
{

    public function getCompanyInfo(): array;

}
