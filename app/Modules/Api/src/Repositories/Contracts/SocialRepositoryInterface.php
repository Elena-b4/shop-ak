<?php


namespace App\Modules\Api\src\Repositories\Contracts;


interface SocialRepositoryInterface
{

    public function getSocials(): array;

}
