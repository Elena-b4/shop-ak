<?php

namespace App\Modules\Api\src\Repositories\Contracts;

interface CartRepositoryInterface
{

    public function getList(): array;

    public function addItemToCart(string $vendorCode, string $count): bool;

    public function deleteItemFromCart(string $vendorCode, string $type): bool;

}
