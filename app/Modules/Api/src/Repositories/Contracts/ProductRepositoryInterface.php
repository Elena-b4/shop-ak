<?php

namespace App\Modules\Api\src\Repositories\Contracts;

use Illuminate\Http\Request;

interface ProductRepositoryInterface
{

    public function getProducts(Request $request, string $slug): ?array;

    public function getProductBySlug(string $slug);

}
