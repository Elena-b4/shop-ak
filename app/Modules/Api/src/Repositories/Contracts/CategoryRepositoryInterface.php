<?php

namespace App\Modules\Api\src\Repositories\Contracts;

interface CategoryRepositoryInterface
{

    public function getCategoryList(): array;

    public function getCategoryBySlug(string $slug): ?array;

}
