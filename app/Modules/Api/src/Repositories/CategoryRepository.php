<?php

namespace App\Modules\Api\src\Repositories;

use App\Modules\Api\src\Models\CategoryApi;
use App\Modules\Api\src\Repositories\Contracts\CategoryRepositoryInterface;
use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\MainEnum;

class CategoryRepository implements CategoryRepositoryInterface
{

    private $ids = [];

    public function getCategoryList(): array
    {
        return CategoryApi::with(['children' => function ($query) {
            $query->orderBy(MainEnum::SORT);
        }])
            ->orderBy(MainEnum::SORT)
            ->where(MainEnum::VISIBLE, true)
            ->where(MainEnum::PARENT_ID, 0)->orderBy(MainEnum::SORT)
            ->select(CategoryEnum::ICON, MainEnum::SLUG, 'id', MainEnum::PARENT_ID,)
            ->get([CategoryEnum::ICON, MainEnum::SLUG, 'id', MainEnum::PARENT_ID])
            ->toArray();
    }

    public function getCategoryBySlug($slug): ?array
    {
        $category = CategoryApi::where(MainEnum::SLUG, $slug)->where(MainEnum::VISIBLE, true)
            ->with('filters', function ($query) {
                $query->where(MainEnum::VISIBLE, true);
            })->first();
        if (!$category) {
            return null;
        }
        return $category->toArray();
    }


    public function getAllChildren($slug): array
    {
        $categories = CategoryApi::where(MainEnum::SLUG, $slug)->with('children')
            ->select(MainEnum::PARENT_ID, 'id')->without('title')->get()->each->setAppends([]);
        $this->getAllCategoriesInside($categories);

        return $this->ids;

    }

    public function getAllCategoriesInside($categories): void
    {
        foreach ($categories as $section) {
            $this->ids[] = $section->id;
            if (isset($section->children)) {
                $this->getAllCategoriesInside($section->children);
            }
        }
    }
}
