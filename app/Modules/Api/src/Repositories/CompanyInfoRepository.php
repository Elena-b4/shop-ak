<?php


namespace App\Modules\Api\src\Repositories;


use App\Modules\Api\src\Models\CompanyInfoApi;
use App\Modules\Api\src\Repositories\Contracts\CompanyInfoRepositoryInterface;
use App\Modules\Database\src\Enums\CompanyInfoEnum;

class CompanyInfoRepository implements CompanyInfoRepositoryInterface
{

    public function getCompanyInfo(): array
    {
        return CompanyInfoApi::find(CompanyInfoEnum::COMPANY_INFO_ID)->toArray();
    }

}
