<?php

namespace App\Modules\Api\src\Repositories;

use App\Modules\Api\src\Models\ReviewApi;
use App\Modules\Api\src\Models\ReviewLikeApi;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ReviewEnum;

class ReviewRepository implements Contracts\ReviewRepositoryInterface
{
    public function getReviewList($productUUID)
    {
        return ReviewApi::where([ReviewEnum::PRODUCT_UUID => $productUUID, ReviewEnum::APPROVED => 1])
            ->with(['children' => function ($query) {
                $query->orderBy(MainEnum::CREATED_AT);
            }])
            ->get([ReviewEnum::USER_NAME, ReviewEnum::CONTENT, 'id', MainEnum::PARENT_ID])
            ->toArray();
    }

    public function getReviewById($reviewId)
    {
        return ReviewApi::find($reviewId);
    }

    public function sendReview($productUUID, $data)
    {
        $review = new ReviewApi();

        $review->user_name = $data['userName'];
        $review->user_email = $data['userEmail'];
        $review->content = $data['content'];
        $review->parent_id = $data['parentId'] ?? 0;
        $review->rating = $data['rating'];
        $review->product_uuid = $productUUID;

        return $review->save();
    }

    public function addLikeOrDislike($review, $data)
    {
        $reviewLike = $this->getReviewLikeByReviewId($review->id);
        if ($reviewLike) {
            if ($reviewLike->value == $data['value']) {
                return $reviewLike->delete();
            } else {
                $reviewLike->value = $data['value'];
            }
        } else {
            $reviewLike = new ReviewLikeApi();
            $reviewLike->review_id = $review->id;
            $reviewLike->user_id = auth()->user()->id;
            $reviewLike->value = $data['value'];
        }
        $reviewLike->save();

        return $reviewLike;
    }

    private function getReviewLikeByReviewId($reviewId)
    {
        return ReviewLikeApi::where(ReviewEnum::REVIEW_ID, $reviewId)->first();
    }
}
