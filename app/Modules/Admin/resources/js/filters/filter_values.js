$(document).ready(function () {

    console.log(21124124);
    let items = product.values,
        allFilters = [{id: '', text: ''}],
        allFiltersValues = [{id: '', text: ''}],
        Controller;

    class SelectController {
        constructor(selectItem, controller) {
            this.controller = controller;
            this.select = selectItem;

            this.setChangeHandler();
        }

        setWidth(width) {
            this.select.select2({
                width: width
            });
        }

        getValue() {
            return this.select.val();
        }

        setOptionsList(setValues) {
            this.select.empty().select2({
                data: setValues
            });
        }

        setChangeHandler() {
            if (typeof this.controller.onChange === 'function') {
                $(this.select).change(() => {
                    this.controller.onChange();
                });
            }
        }
    }

    class FilterClass {
        constructor() {
            this.selectController = new SelectController($('#filters'), this);
            product.filters.forEach(function (filter) {
                let values = [];

                allFilters.push({
                    id: filter.id,
                    text: filter.title,
                });

                filter.values.forEach(function (value) {

                    if (!items.find(item => item.id === value.id)) {
                        values.push({
                            id: value.id,
                            text: value.title,
                        });
                    }
                });

                if (values.length) {
                    allFiltersValues[filter.id] = values;
                }

            });

            this.selectController.setWidth('200px');
            this.selectController.setOptionsList(allFilters);
        }

        onChange() {
            let filterValuesController = Controller.getController(FilterValuesClass);
            filterValuesController.setValues();
        }

        getSelectController() {
            return this.selectController;
        }
    }

    class FilterValuesClass {
        constructor() {
            this.selectController = new SelectController($('#filterValues'), this);

            product.filters.forEach(function (filter) {
                allFilters.push({
                    id: filter.id,
                    text: filter.title,
                });
            });

            this.selectController.setWidth('200px');
            this.selectController.setOptionsList(allFilters);
            this.setValues();
        }

        setValues() {
            let filterSelectController = Controller.getController(FilterClass).getSelectController(),
                filterId = filterSelectController.getValue(),
                setOptions;

            if (filterId) {
                setOptions = allFiltersValues[filterId];
            } else {
                setOptions = allFiltersValues.length ? allFiltersValues[0] : [];
            }

            this.selectController.setOptionsList(setOptions);
        }
    }

    class ControllerClass {
        constructor() {
            this.controllers = [];
        }

        addController(className) {
            this.controllers[className] = new className();
        }

        getController(className) {
            if (this.controllers[className] !== undefined) {
                return this.controllers[className];
            }
            return false;
        }
    }

    Controller = new ControllerClass();
    Controller.addController(FilterClass)
    Controller.addController(FilterValuesClass)

});
