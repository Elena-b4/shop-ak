$(document).ready(function () {
    let $adminLocaleWrapper = $('#admin-locale-select');

    $adminLocaleWrapper.click(function () {
        $(this).toggleClass('open');
    });

    $('li', $adminLocaleWrapper).click(function () {
        let setLang = $(this).text();

        $.ajax({
            url: $adminLocaleWrapper.find('.locale-list').data('action'),
            cache: false,
            type: "POST",
            data: {
                lang: setLang
            },
            success: function(response) {
                $adminLocaleWrapper.find('.current-locate').text(setLang);
                document.location.reload();
            }
        });
    });
});
