<script>
    $(document).ready(function () {
        let properties = JSON.parse(@json($data['properties'])),
            productProps = JSON.parse(@json($data['product_props'])),
            availableProperties = [{id: '', text: ''}],
            Controller;

        class SelectController {
            constructor(selectItem, controller) {
                this.controller = controller;
                this.select = selectItem;

                this.setChangeHandler();
            }
            setWidth(width) {
                this.select.select2({
                    width: width
                });
            }
            getValue() {
                return this.select.val();
            }
            setOptionsList(setValues) {
                this.select.empty().select2({
                    data: setValues
                });
            }
            setChangeHandler() {
                if (typeof this.controller.onChange === 'function') {
                    $(this.select).change(() => {
                        this.controller.onChange();
                    });
                }
            }
        }

        class PropertyClass {
            constructor() {
                this.selectController = new SelectController($('#properties'), this);
                properties.forEach(function (property) {
                    if (!productProps.find(x => x.id === property.id)) {
                        availableProperties.push({
                            id: property.id,
                            text: property.title,
                        });
                    }
                });

                this.selectController.setWidth('200px');
                this.selectController.setOptionsList(availableProperties);
            }
            getSelectController() {
                return this.selectController;
            }
        }

        class ControllerClass {
            constructor() {
                this.controllers = [];
            }
            addController(className) {
                this.controllers[className] = new className();
            }
            getController(className) {
                if (this.controllers[className] !== undefined) {
                    return this.controllers[className];
                }
                return false;
            }
        }

        Controller = new ControllerClass();
        Controller.addController(PropertyClass)

    });
</script>
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Добавить</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body" style="display: block;">
            <form method="POST" action="{{ route('createProductProperty') }}" class="form-horizontal" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="box-body fields-group">
                    <input type="hidden" name="product_id" value="{{ $product_id }}">

                    <div class="form-group">
                        <label for="type" class="col-sm-2  control-label">Type</label>
                        <div class="col-sm-8">
                            <select id="properties" name="property_id" class="form-control type select2-hidden-accessible" tabindex="-1" aria-hidden="true"></select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="value" class="col-sm-2  control-label">Value</label>
                        <div class="col-sm-8">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-pencil fa-fw"></i></span>
                                <input type="text" id="value" name="value" value="" class="form-control value" placeholder="Ввод Value">
                            </div>
                        </div>
                    </div>

                </div>

                <div class="box-footer">
                    <div class="col-md-2"></div>

                    <div class="col-md-8">
                        <div class="btn-group pull-left">
                            <button type="reset" class="btn btn-warning pull-right">Сбросить</button>
                        </div>

                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-info pull-right">Отправить</button>
                        </div>
                    </div>
                </div>
            </form>

        </div><!-- /.box-body -->
    </div>

</div>
