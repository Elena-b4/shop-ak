<script>
    $(document).ready(function () {
        let product = JSON.parse(@json($data['product']));
        let items = product.values,
            allFilters = [{id: '', text: ''}],
            allFiltersValues = [{id: '', text: ''}],
            Controller;

        class SelectController {
            constructor(selectItem, controller) {
                this.controller = controller;
                this.select = selectItem;

                this.setChangeHandler();
            }

            setWidth(width) {
                this.select.select2({
                    width: width
                });
            }

            getValue() {
                return this.select.val();
            }

            setOptionsList(setValues) {
                this.select.empty().select2({
                    data: setValues
                });
            }

            setChangeHandler() {
                if (typeof this.controller.onChange === 'function') {
                    $(this.select).change(() => {
                        this.controller.onChange();
                    });
                }
            }
        }

        class FilterClass {
            constructor() {
                this.selectController = new SelectController($('#filters'), this);
                product.filters.forEach(function (filter) {
                    let values = [];

                    filter.values.forEach(function (value) {
                        if (!items.find(item => item.id === value.id)) {
                            values.push({
                                id: value.id,
                                text: value.title,
                            });
                        }
                    });

                    if (values.length) {
                        allFiltersValues[filter.id] = values;

                        allFilters.push({
                            id: filter.id,
                            text: filter.title,
                        });
                    }

                });

                this.selectController.setWidth('200px');
                this.selectController.setOptionsList(allFilters);
            }

            onChange() {
                let filterValuesController = Controller.getController(FilterValuesClass);
                filterValuesController.setValues();
            }

            getSelectController() {
                return this.selectController;
            }
        }

        class FilterValuesClass {
            constructor() {
                this.selectController = new SelectController($('#filterValues'), this);

                product.filters.forEach(function (filter) {
                    allFilters.push({
                        id: filter.id,
                        text: filter.title,
                    });
                });

                this.selectController.setWidth('200px');
                this.selectController.setOptionsList(allFilters);
                this.setValues();
            }

            setValues() {
                let filterSelectController = Controller.getController(FilterClass).getSelectController(),
                    filterId = filterSelectController.getValue(),
                    setOptions;

                if (filterId) {
                    setOptions = allFiltersValues[filterId];
                } else {
                    setOptions = allFiltersValues.length ? allFiltersValues[0] : [];
                }

                this.selectController.setOptionsList(setOptions);
            }
        }

        class ControllerClass {
            constructor() {
                this.controllers = [];
            }

            addController(className) {
                this.controllers[className] = new className();
            }

            getController(className) {
                if (this.controllers[className] !== undefined) {
                    return this.controllers[className];
                }
                return false;
            }
        }

        Controller = new ControllerClass();
        Controller.addController(FilterClass)
        Controller.addController(FilterValuesClass)

    });
</script>
<div class="col-md-12">
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">Добавить</h3>
            <div class="box-tools pull-right">
            </div><!-- /.box-tools -->
        </div><!-- /.box-header -->
        <div class="box-body" style="display: block;">
            <form method="POST" action="{{ route('createProductFilterValue', ['product_id' => $product_id]) }}" class="form-horizontal"
                  accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="box-body fields-group">

                    <div class="form-group">
                        <label for="type" class="col-sm-2  control-label">Type</label>
                        <div class="col-sm-8">
                            <select id="filters" class="form-control type select2-hidden-accessible" tabindex="-1"
                                    aria-hidden="true"></select>
                        </div>
                    </div>
                    <div class="form-group  ">
                        <label for="type" class="col-sm-2  control-label">Values</label>
                        <div class="col-sm-8">
                            <select id="filterValues" name="filter_value_id"
                                    class="form-control type select2-hidden-accessible" tabindex="-1"
                                    aria-hidden="true"></select>
                        </div>
                    </div>

                </div>

                <input type="hidden" name="_token" value="8jZF1DUk00lRYxt0FqfJlKqeQBPKBj7LgqNE4NvP"><!-- /.box-body -->
                <div class="box-footer">
                    <div class="col-md-2"></div>

                    <div class="col-md-8">
                        <div class="btn-group pull-left">
                            <button type="reset" class="btn btn-warning pull-right">Сбросить</button>
                        </div>

                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-info pull-right">Отправить</button>
                        </div>
                    </div>
                </div>
            </form>

        </div><!-- /.box-body -->
    </div>

</div>
