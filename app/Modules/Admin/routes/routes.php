<?php

use App\Modules\Admin\src\Controllers\AdvertisingController;
use App\Modules\Admin\src\Controllers\AdvertisingPageBlockController;
use App\Modules\Admin\src\Controllers\Categories\CrudCategoriesController;
use App\Modules\Admin\src\Controllers\CompanyInfoController;
use App\Modules\Admin\src\Controllers\DeparturePoints\CrudDeparturePointsController;
use App\Modules\Admin\src\Controllers\FiltersController;
use App\Modules\Admin\src\Controllers\LangController;
use App\Modules\Admin\src\Controllers\MainController;
use App\Modules\Admin\src\Controllers\MenuController;
use App\Modules\Admin\src\Controllers\OrderItems\PrepareOrdersController;
use App\Modules\Admin\src\Controllers\OrderItems\WishListController;
use App\Modules\Admin\src\Controllers\Product\BrandController;
use App\Modules\Admin\src\Controllers\Product\CkeditorUploadImgController;
use App\Modules\Admin\src\Controllers\Product\ProductController;
use App\Modules\Admin\src\Controllers\Product\ImagesController;
use App\Modules\Admin\src\Controllers\Product\ItemController;
use App\Modules\Admin\src\Controllers\Product\ProductFilterController;
use App\Modules\Admin\src\Controllers\Product\ProductImagesController;
use App\Modules\Admin\src\Controllers\Product\ProductPropertiesController;
use App\Modules\Admin\src\Controllers\Product\PropertiesController;
use App\Modules\Admin\src\Controllers\SocialsController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

Admin::routes();

Route::group([
    'prefix' => config('admin.route.prefix'),
    'middleware' => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', [MainController::class, 'index'])->name('home');
    $router->resource('/company', CompanyInfoController::class);
    $router->resource('/socials', SocialsController::class);


//products
    $router->resource('/product', ProductController::class);
    $router->resource('/brand', BrandController::class);
    $router->post('product-filter-values/create', [ProductFilterController::class, 'createProductFilterValue'])->name('createProductFilterValue');
    $router->get('create-from-current/{product_id}', [ProductController::class, 'createFromCurrentProduct'])->name('createFromCurrentProduct');

//advertising
    $router->resource('/advertising', AdvertisingController::class);
    $router->resource('/{advert_id}/advertising-page-block', AdvertisingPageBlockController::class, [
        'names' => [
            'index' => 'page-block.index',
            'store' => 'page-block.store',
            'destroy' => 'page-block.destroy',
            'update' => 'page-block.update',
            'edit' => 'page-block.edit',
        ]
    ]);
    $router->get('/add-advert-block/{advert_id}', [AdvertisingController::class, 'addAdvertBlock'])->name('addAdvertBlock');


//properties
    $router->resource('/properties', PropertiesController::class);

//product properties
    $router->resource('{product_id}/product-properties', ProductPropertiesController::class);
    $router->post('product-properties/create', [ProductPropertiesController::class, 'createProductProperty'])->name('createProductProperty');

//items
    $router->resource('{product_id}/items', ItemController::class);

//images
    $router->resource('{item_id}/images', ImagesController::class, [
        'names' => [
            'index' => 'item-images.index',
            'store' => 'item-images.store',
            'destroy' => 'item-images.destroy',
            'update' => 'item-images.update',
            'edit' => 'item-images.edit',
        ]
    ]);

    //images product
    $router->resource('{product_id}/product-images', ProductImagesController::class, [
        'names' => [
            'index' => 'product-images.index',
            'store' => 'product-images.store',
            'destroy' => 'product-images.destroy',
            'update' => 'product-images.update',
            'edit' => 'product-images.edit',
        ]
    ]);
    $router->delete('{product_id}/product-images/{file_id}', [ProductImagesController::class, 'destroy']);

//filters for items
    $router->resource('{product_id}/product-filter-values', ProductFilterController::class);


//filters
    $router->resource('/filters', FiltersController::class);

//categories
    $router->resource('/categories', CrudCategoriesController::class);

// language
    $router->resource('/lang', LangController::class);
    $router->post('/set-locale', [LangController::class, 'setLocale'])->name('setLocale');;

//menu
    $router->resource('/menu', MenuController::class);
    $router->post('/menu/tree', [MenuController::class, 'tree'])->name('menuTree');

//departure points
    $router->resource('/departure-points', CrudDeparturePointsController::class);


    $router->post('/product/ckeditor/upload', [CkeditorUploadImgController::class, 'upload'])->name('ckeditor');



    //orders
    Route::group([
        'prefix' => 'order',
    ], function (Router $router) {
        //grid table view
        $router->get('/wishList/list', [WishListController::class, 'list'])->name('order.list');
        //post request items in table with pagination
        $router->get('/wishList/items', [WishListController::class, 'items'])->name('order.items');
        //post search products by product name or vendor code
        //TODO add search by bar_code
        $router->post('/wishList/searchItems', [WishListController::class, 'getProductWithItems'])->name('order.getProductWithItems');
        //set need_order in true
        $router->put('/wishList/addItem/{item_id}', [WishListController::class, 'updateItemNeedOrder'])->name('order.updateItemNeedOrder');
        //add item to prepare order
        $router->post('/wishList/addItemToPrepareOrder', [WishListController::class, 'addItemToPrepareOrder'])->name('order.addItemToPrepareOrder');


        $router->get('/orderItems/list', [PrepareOrdersController::class, 'list'])->name('order.prepare.list');
        $router->get('/prepare-orders/items', [PrepareOrdersController::class, 'items'])->name('order.prepare.items');

    });
});
