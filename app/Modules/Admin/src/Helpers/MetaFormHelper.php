<?php

namespace App\Modules\Admin\src\Helpers;

use App\Modules\Database\src\Enums\MainEnum;
use Encore\Admin\Form;

class MetaFormHelper
{

    public static function getMetaForm(Form $form, bool $disableTag = false, bool $disableDescription = false) : void
    {
        if(!$disableTag){
            $form->embeds(MainEnum::META_TAG, __(MainEnum::LANG_GROUP . MainEnum::META_TAG), function ($form) {
                foreach (MainEnum::getLangs() as $lang) {
                    $form->text(MainEnum::META_TAG . $lang, __(MainEnum::LANG_GROUP . MainEnum::META_TAG) . ' ' . $lang);
                }
            });
        }

        if (!$disableDescription){
            $form->embeds(MainEnum::META_DESCRIPTION, __(MainEnum::LANG_GROUP . MainEnum::META_DESCRIPTION), function ($form) {
                foreach (MainEnum::getLangs() as $lang) {
                    $form->text(MainEnum::META_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . MainEnum::META_DESCRIPTION) . ' ' . $lang);
                }
            });
        }
    }

}
