<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Filter;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Widgets\Table;

class FiltersController extends AdminController
{
    protected function grid() :Grid
    {
        $grid = new Grid(new Filter());

        $grid->column('id', __('ID'))->sortable();
        $grid->column(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG))->sortable();
        $grid->column('title', __(MainEnum::LANG_GROUP . MainEnum::TITLE))->display(function () {
            return $this->title;
        });
        $grid->column(MainEnum::SORT, __(MainEnum::LANG_GROUP . MainEnum::SORT))->sortable()->editable();
        $grid->column(MainEnum::VISIBLE, __(MainEnum::LANG_GROUP . MainEnum::VISIBLE))->bool();
        $grid->column('', '')->expand(function ($model) {
            $values = $model->values()->get()->map(function ($value) {
                return $value->only(['id', MainEnum::SLUG, MainEnum::TITLE, MainEnum::VALUE]);
            });

            return new Table([
                'ID',
                __(MainEnum::LANG_GROUP . MainEnum::SLUG),
                __(MainEnum::LANG_GROUP . MainEnum::TITLE),
            ], $values->toArray());
        });

        return $grid;
    }


    public function form() {

        $form = new Form(new Filter());

        $form->display('id', __('ID'));
        $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG));
        $form->number(MainEnum::SORT, __(MainEnum::LANG_GROUP . MainEnum::SORT))->default(Filter::count() + 1);
        $form->radio(MainEnum::VISIBLE,MainEnum::LANG_GROUP . MainEnum::VISIBLE)->options([0 => 'No', 1=> 'Yes'])->default(0);
        $form->hasMany(MainEnum::VALUES, __(MainEnum::LANG_GROUP . MainEnum::VALUES), function (Form\NestedForm $form) {
            $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG));
            $form->text(MainEnum::VALUE, __(MainEnum::LANG_GROUP . MainEnum::VALUE));
        });

        return $form;

    }

}
