<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ProductPropertyEnum;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductProperties;
use App\Modules\Database\src\Models\Property;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;

class ProductPropertiesController extends AdminController
{

    protected $title = 'Product properties';

    protected function grid($product_id): Grid
    {
        $grid = new Grid(new ProductProperties());
        $grid->model()->where(ProductEnum::PRODUCT_ID, $product_id);

        $grid->column('id', __('ID'))->sortable();
        $grid->column('title', __(MainEnum::LANG_GROUP . MainEnum::TITLE))->display(function () {
            return Property::find($this->property_id)->title;
        });
        $grid->column(MainEnum::VALUE, __(MainEnum::LANG_GROUP . MainEnum::VALUE))->sortable();

        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableView();
        });
        $grid->disableCreateButton();

        return $grid;
    }

    public function index(Content $content, $product_id = null)
    {
        return $content
            ->title(__(MainEnum::LANG_GROUP . ProductPropertyEnum::TABLE_NAME))
            ->row(function (Row $row) use ($product_id) {
                $row->column(6, function (Column $column) use ($product_id) {
                    $column->append($this->grid($product_id));
                });

                $row->column(6, function (Column $column) use ($product_id) {
                    $data = $this->getVariables($product_id);
                    $column->append(view('admin::product.properties.add')->with(compact('data', 'product_id')));
                });
            });
    }

    protected function getVariables($productId) :array
    {
        $properties = Property::all();
        $product = Product::where('id', $productId)->with('properties')->first();
        $data['properties'] = $properties->toJson();
        $data['product_props'] = $product->properties->toJson();
        return $data;
    }

    public function createProductProperty(Request $request) {

        $user = new ProductProperties();
        $user->product_id= $request->product_id;
        $user->property_id = $request->property_id;
        $user->value = $request->value;
        $user->save();

        return redirect(route('product-properties.index', ['product_id' => $request->product_id]));

    }

}
