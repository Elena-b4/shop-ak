<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Admin\src\Models\Socials;
use App\Modules\Admin\src\Traits\SortTrait;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\SocialEnum;
use App\Modules\Database\src\Models\Property;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Illuminate\Database\Eloquent\Model;

class PropertiesController extends AdminController
{
    use SortTrait;

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Properties';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() :Grid
    {
        $grid = new Grid(new Property());
        $grid->model()->orderBy(MainEnum::SORT);

        $grid->column('id', __('ID'))->sortable();
        $grid->column(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG))->sortable();
        $grid->column('title', __(MainEnum::LANG_GROUP . MainEnum::TITLE))->display(function () {
            return $this->title;
        });
        $grid->visible(__(MainEnum::LANG_GROUP . MainEnum::VISIBLE))->display(function ($visible) {
            return $visible ? 'Yes' : 'No';
        });
        $grid->column(MainEnum::SORT, __(MainEnum::SORT))->sortable()->editable();

        return $grid;
    }

    public function form()
    {
        $form = new Form(new Property());

        $form->display('id', __('ID'));
        $form->text(MainEnum::SLUG, __(MainEnum::SLUG))->rules('required');
        $form->radio(MainEnum::VISIBLE,MainEnum::LANG_GROUP . MainEnum::VISIBLE)->options([0 => 'No', 1=> 'Yes'])->default(0);
        $this->sort($form, MainEnum::MODEL_PROPERTY);

        return $form;
    }

}
