<?php


namespace App\Modules\Admin\src\Controllers\Product;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CkeditorUploadImgController
{

    public function upload(Request $request)
    {

        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName . '_' . time() . '.' . $extension;

            $request->file('upload')->move(public_path('images'), $fileName);

            $url = asset('images/' . $fileName);
            return response()->json([ 'uploaded' => 1, 'fileName' => $fileName, 'url' => $url]);
        }
    }

}
