<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Database\src\Enums\BrandEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Brand;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class BrandController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Brand';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Brand());
        $grid->model()->orderBy('id');

        $grid->column('id', __('ID'))->sortable();
        $grid->column(BrandEnum::NAME, __('table.' . BrandEnum::NAME))->sortable()->editable();
        $grid->column(BrandEnum::COUNTRY, __('table.' . BrandEnum::COUNTRY))->sortable()->editable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }


    public function form()
    {
        $form = new Form(new Brand());

        $form->display('id', __('ID'));
        $form->text(BrandEnum::NAME, __('table.' . BrandEnum::NAME));
        $form->text(BrandEnum::COUNTRY, __('table.' . BrandEnum::COUNTRY));

        return $form;
    }

}
