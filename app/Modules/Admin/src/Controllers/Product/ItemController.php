<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\Item;
use App\Modules\Database\src\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class ItemController extends AdminController
{

    protected $title = 'Items';

    protected function grid($product_id): Grid
    {
        $grid = new Grid(new Item());
        $grid->model()->where(ProductEnum::PRODUCT_ID, $product_id)->orderBy('id');

        $grid->column('id', __('ID'))->sortable();
        $grid->column(ItemEnum::SIZE, __(MainEnum::LANG_GROUP . ItemEnum::SIZE))->sortable()->editable();
        $grid->column(ItemEnum::PRICE, __(MainEnum::LANG_GROUP . ItemEnum::PRICE))->sortable();
        $grid->column(ItemEnum::DISCOUNT, __(MainEnum::LANG_GROUP . ItemEnum::DISCOUNT))->sortable();
        $grid->column('links', "Ссылки")->display(function () use ($grid)  {
            return '<p><a href="/admin/'.$this->id.'/images">Картинки</a></p>';
        });

        return $grid;
    }

    public function edit($product_id, Content $content, $id = null)
    {
        $item = Item::find($id);
        if (!$item) {
            abort(404);
        }
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($product_id, $id)->edit($id));
    }

    public function create(Content $content, $product_id = null)
    {
        $product = Product::find($product_id);
        if (!$product) {
            abort(404);
        }
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form($product_id));
    }

    public function index(Content $content, $product_id = null)
    {
        $product = Product::find($product_id);
        if (!$product) {
            abort(404);
        }
        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid($product_id));
    }

    public function form($product_id = null, $id = false)
    {

        $form = new Form(new Item());

        if ($id) {
            $form->tools(function (Form\Tools $tools) use ($id) {
                $tools->disableView();
                $tools->add('<a class="btn btn-sm btn-info" href="' . route('item-images.index', ['item_id' => $id]) . '"><i class="fa fa-product-hunt"></i>&nbsp;&nbsp;' . __('table.images') . '</a>');
            });
        }

        $form->hidden(ProductEnum::PRODUCT_ID)->value($product_id);
        $form->currency(ItemEnum::PRICE, __(MainEnum::LANG_GROUP . ItemEnum::PRICE))->symbol(MainEnum::COIN_ICON_BY);
        $form->text(ItemEnum::VENDOR_CODE, __(MainEnum::LANG_GROUP . ItemEnum::VENDOR_CODE));
        $form->number(ItemEnum::DISCOUNT, __(MainEnum::LANG_GROUP . ItemEnum::DISCOUNT) . ' in %')->pattern('[0-9]{1,2}');
        $form->number(ItemEnum::QUANTITY, __(MainEnum::LANG_GROUP . ItemEnum::QUANTITY));
        $form->text(ItemEnum::SIZE, __(MainEnum::LANG_GROUP . ItemEnum::SIZE));

        $form->saving(function (Form $form) use ($product_id) {
            $product = Product::find($form->product_id);
            if ($product && $form->price < $product->price) {
                $product->price = $form->price;
                $product->save();
            }
        });

        return $form;
    }


    public function update($productId, $id = null)
    {
        return $this->form()->update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($itemId, $id = null)
    {
        return $this->form()->destroy($id);
    }

}
