<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Admin\src\Traits\SortTrait;
use App\Modules\Common\src\Helpers\S3;
use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\File;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;

class ImagesController extends AdminController
{
    use SortTrait;

    public function index(Content $content, $id = null)
    {
        return $content
            ->title(__(MainEnum::LANG_GROUP . FileEnum::TABLE_NAME))
            ->row(function (Row $row) use ($id) {

                $row->column(6, function (Column $column) use ($id) {
                    $column->append(File::tree(function ($tree) use ($id) {
                        $tree->query(function ($model) use ($id) {
                            return $model->where('entity_id', $id)
                                ->where('entity', ItemEnum::TABLE_NAME)
                                ;
                        });
                        $views = array(
                            'tree' => 'admin::partials.tree',
                            'branch' => 'admin::tree.branch');
                        $tree->setView($views);
                        $tree->nestable(["maxDepth" => 1]);

                        $tree->branch(function ($branch) {
                            $url = S3::getUrl($branch['name']);

                            $logo = "<img src='$url' style='max-width:220px;max-height:220px' class='img'/>";
                            if ($branch['visible'] === true) {
                                $string = "<div class='btn btn-success'>Active</div>";
                            } else {
                                $string = "<a><div class='btn btn-danger'>Disable</div></a>";

                            }
                            return " $logo  {$branch['id']} - " . $string;
                        });
                    }));
                });

                $row->column(6, function (Column $column) use ($id) {
                    $form = new \Encore\Admin\Widgets\Form();
                    $form->action(route('item-images.index', ['item_id' => $id]));

                    $form->file(FileEnum::NAME, __(MainEnum::LANG_GROUP . FileEnum::NAME));
                    $form->radio(MainEnum::VISIBLE, __(MainEnum::LANG_GROUP . MainEnum::VISIBLE))->options([0 => 'false', 1 => 'true'])->default(1);
                    $this->sortFiles($form, FileEnum::MODEL, ProductEnum::TABLE_NAME, $id);
                    $form->hidden(FileEnum::ENTITY_ID)->default($id);
                    $form->hidden(FileEnum::ENTITY)->default(ItemEnum::TABLE_NAME);
                    $form->select(FileEnum::TYPE)->options(MainEnum::getFileTypes())->default('img');


                    $column->append((new Box(__('admin.new'), $form))->style('success'));
                });
            });
    }

    public function edit($item_id, Content $content, $id = null)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($item_id, $id)->edit($id));
    }

    public function create(Content $content, $item_id = null)
    {
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form($item_id));
    }


    public function form($item_id = null, $id = false)
    {


        $form = new Form(new File());
        $form->file(FileEnum::NAME, __(MainEnum::LANG_GROUP . FileEnum::NAME));
        $form->radio(MainEnum::VISIBLE, __(MainEnum::LANG_GROUP . MainEnum::VISIBLE))->options([0 => 'false', 1 => 'true'])->default(1);
        $this->sort($form, FileEnum::MODEL);
        $form->hidden(FileEnum::ENTITY_ID)->default($item_id);
        $form->hidden(FileEnum::ENTITY)->default(FileEnum::TABLE_NAME);
        $form->select(FileEnum::TYPE)->options(MainEnum::getFileTypes());

        return $form;

    }

    public function destroy($productId, $imageId = null)
    {
        return $this->form()->destroy($imageId);
    }
}
