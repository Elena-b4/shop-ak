<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Admin\src\Grid\ProductRow;
use App\Modules\Admin\src\Selectable\FilterSelectable;
use App\Modules\Admin\src\Traits\SortTrait;
use App\Modules\Common\src\Helpers\S3;
use App\Modules\Database\src\Enums\BrandEnum;
use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\PropertyEnum;
use App\Modules\Database\src\Models\Brand;
use App\Modules\Database\src\Models\Category;
use App\Modules\Database\src\Models\File;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductNames;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Str;

class ProductController extends AdminController
{
    use SortTrait;

    protected $title = 'Products';

    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($id)->edit($id));
    }

    public function form($id = false)
    {

        $form = new Form(new Product());
        $form->text(ProductEnum::NAME, 'Main name in english');

        if ($id) {
            $productNames = ProductNames::where(ProductEnum::PRODUCT_ID, $id)->get();

            foreach ($productNames as $productName) {
                if (in_array($productName->lang, MainEnum::getLangs())) {
                    $form->text($productName->lang, __(MainEnum::LANG_GROUP . ProductEnum::NAME) . ' ' . $productName->lang)->value($productName->name);
                }
            }
            $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG). ' empty if automatic');
            $form->text(ProductEnum::VENDOR_CODE, __(MainEnum::LANG_GROUP . ProductEnum::VENDOR_CODE));

            $form->tools(function (Form\Tools $tools) use ($id) {
                $tools->disableView();

                $tools->add('<a class="btn btn-sm btn-info" href="' . route('product-properties.index', ['product_id' => $id]) . '"><i class="fa ' . PropertyEnum::FA_ICON . '"></i>&nbsp;&nbsp;' . __(MainEnum::LANG_GROUP . 'properties') . '</a>');
                $tools->add('<a class="btn btn-sm btn-info" href="' . route('product-filter-values.index', ['product_id' => $id]) . '"><i class="fa fa-product-hunt"></i>&nbsp;&nbsp;' . __('table.filter_values') . '</a>');
                $tools->add('<a class="btn btn-sm btn-info" href="' . route('items.index', ['product_id' => $id]) . '"><i class="fa fa-product-hunt"></i>&nbsp;&nbsp;' . __(MainEnum::LANG_GROUP . 'item') . '</a>');
                $tools->add('<a class="btn btn-sm btn-info" href="' . route('product-images.index', ['product_id' => $id]) . '"><i class="fa fa-product-hunt"></i>&nbsp;&nbsp;' . __('table.images') . '</a>');
                $tools->add('<a class="btn btn-sm btn-info" href="' . route('createFromCurrentProduct', ['product_id' => $id]) . '"><i class="fa fa-product-hunt"></i>&nbsp;&nbsp;' . __('table.create-from-current') . '</a>');
            });
        } else {
            $form->hidden(ProductEnum::UUID)->value(Str::uuid());
            foreach (MainEnum::getLangs() as $lang) {
                $form->text($lang, __(MainEnum::LANG_GROUP . ProductEnum::NAME) . ' ' . $lang);
            }
        }

        $form->display('id', __('ID'));
        $this->sort($form, ProductEnum::MODEL);
        $form->select(ProductEnum::COLOR, __(MainEnum::LANG_GROUP . ProductEnum::COLOR))->options(MainEnum::getColors());
        $form->currency(ProductEnum::PRICE, __(MainEnum::LANG_GROUP . ProductEnum::PRICE))->symbol(MainEnum::COIN_ICON_BY);;
        $form->radio(MainEnum::VISIBLE, __(MainEnum::LANG_GROUP . MainEnum::VISIBLE))->options([0 => 'false', 1 => 'true'])->default(1);
        $form->radio(ProductEnum::HIT, __(MainEnum::LANG_GROUP . ProductEnum::HIT))->options([0 => 'false', 1 => 'true'])->default(0);
        $form->radio(ProductEnum::NEW, __(MainEnum::LANG_GROUP . ProductEnum::NEW))->options([0 => 'false', 1 => 'true'])->default(0);
        $form->select(ProductEnum::BRAND_ID, __(MainEnum::LANG_GROUP . ProductEnum::BRAND_ID))->options(Brand::all()->pluck(BrandEnum::NAME, 'id'));
        $form->multipleSelect(CategoryEnum::TABLE_NAME, __(MainEnum::LANG_GROUP . CategoryEnum::TABLE_NAME))->options(Category::all()->pluck(MainEnum::TITLE, 'id'));


        $form->embeds(ProductEnum::SHORT_DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->textarea(ProductEnum::SHORT_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . ProductEnum::SHORT_DESCRIPTION) . ' ' . $lang)->rows(4);
            }
        });

        $form->embeds(ProductEnum::DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->ckeditor(ProductEnum::DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . ProductEnum::DESCRIPTION) . ' ' . $lang);
            }
        });

        $form->embeds(MainEnum::TITLE, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(MainEnum::TITLE . $lang, __(MainEnum::LANG_GROUP . MainEnum::TITLE) . ' ' . $lang);
            }
        });

        $form->embeds(ProductEnum::META_TAG, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(ProductEnum::META_TAG . $lang, __(MainEnum::LANG_GROUP . ProductEnum::META_TAG) . ' ' . $lang);
            }
        });

        $form->embeds(ProductEnum::META_DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(ProductEnum::META_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . ProductEnum::META_DESCRIPTION) . ' ' . $lang);
            }
        });


        $form->belongsToMany('filters', FilterSelectable::class, __(MainEnum::LANG_GROUP . FilterEnum::TABLE_NAME));

        $form->saved(function (Form $form) {

            foreach (MainEnum::getLangs() as $lang) {
                ProductNames::updateOrCreate([
                    MainEnum::LANG => $lang,
                    ProductEnum::PRODUCT_ID => $form->model()->id],
                    [ProductEnum::NAME => request($lang)]
                );
            }

        });
        $form->ignore(array_values(MainEnum::getLangs()));

        return $form;

    }

    public function createFromCurrentProduct($product_id)
    {
        /** @var Product $product*/
        $product = Product::find($product_id)->makeHidden('id');

        $newProduct = new Product();
        $newProduct->short_description = $product->short_description;
        $newProduct->meta_description = $product->meta_description;
        $newProduct->title = $product->title;
        $newProduct->description = $product->description;
        $newProduct->meta_tag = $product->meta_tag;
        $newProduct->name = $product->name;

        $newProduct->price = $product->price;
        $newProduct->is_new = $product->is_new;
        $newProduct->visible = $product->visible;
        $newProduct->brand_id = $product->brand_id;
        $newProduct->uuid = $product->uuid;
        $newProduct->sort = Product::count() + 1;

        $newProduct->save();

        foreach ($product->names as $name) {
            $name = $name->toArray();
            unset($name['id']);
            unset($name['product_id']);
            $name['product_id'] = $newProduct->id;
            ProductNames::create($name);
        }
        $newProduct->filters()->attach($product->filters()->get());
        $newProduct->values()->attach($product->values()->get());

        return redirect()->route('product.edit', ['product' => $newProduct->id]);
    }

    protected function grid(): Grid
    {
        $grid = new Grid(new Product());
        $grid->model()->orderBy(MainEnum::SORT);

        $grid->actions(function ($actions) {
            $actions->add(new ProductRow());
            $actions->disableView();
            // append an action.

        });
        $grid->column('image', "Изображение")->display(function () use ($grid)  {

            $file = File::where('entity', FileEnum::TABLE_NAME)
                ->where('entity_id', $this->id)
                ->where('type', 'img')
                ->first();

            if ($file) {
                $fileUrl = S3::getUrl($file->name);
                return "<img src='$fileUrl' style='max-width:300px;max-height:300px' class='img'/>";
            }

            return '';
        });

        $grid->column(MainEnum::SLUG);
        $grid->column(ProductEnum::NAME);
        $lang = app()->getLocale();
//        $grid->column('id', 'ID  :  ' . __(MainEnum::LANG_GROUP . ProductEnum::NAME))->sortable()->display(function ($id) use ($lang) {
//            return $id . ' : ' . ProductNames::where(ProductEnum::PRODUCT_ID, $id)->where(MainEnum::LANG, $lang)->get()->first()->name ?? '';
//        });

        $grid->column(ProductEnum::BRAND_ID, __(MainEnum::LANG_GROUP . 'brand'))->sortable()->display(function ($id) {
            return Brand::find($id)->name ?? __(MainEnum::LANG_GROUP . 'withoutBrand');
        });
        $grid->column(ProductEnum::PRICE, __(MainEnum::LANG_GROUP . ProductEnum::PRICE))->sortable()->editable();
        $grid->column(MainEnum::SORT, __(MainEnum::LANG_GROUP . MainEnum::SORT))->sortable()->editable();
        $grid->column(ProductEnum::COLOR, __(MainEnum::LANG_GROUP . ProductEnum::COLOR))->sortable()->editable();
        $grid->username('Username');

        $grid->column('links', "Ссылки")->display(function () use ($grid)  {

            return '<p><a href="/admin/'.$this->id.'/product-properties">Свойства</a></p>
                    <p><a href="/admin/'.$this->id.'/product-filter-values">Фильтры</a></p>
                    <p><a href="/admin/'.$this->id.'/items">Товары</a></p>
                    <p><a href="/admin/'.$this->id.'/product-images">Материалы</a></p>
                    <p><a href="/admin/create-from-current/'.$this->id.'">Создать от текущего</a></p>
'
                ;
        });

        return $grid;
    }


}
