<?php

namespace App\Modules\Admin\src\Controllers\Product;

use App\Modules\Admin\src\Traits\SortTrait;
use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\Filter;
use App\Modules\Database\src\Models\FilterValue;
use App\Modules\Database\src\Models\Product;
use App\Modules\Database\src\Models\ProductFilterValue;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;

class ProductFilterController extends AdminController
{
    use SortTrait;

    protected function grid($product_id): Grid
    {
        $grid = new Grid(new ProductFilterValue());
        $filterId = $grid->model();
        $grid->model()->where(ProductEnum::PRODUCT_ID, $product_id);

        $grid->column('id', __('ID'))->sortable();

        $grid->column(FilterValueEnum::FILTER_VALUE_ID, __(MainEnum::LANG_GROUP . FilterValueEnum::FILTER_VALUE_ID))->sortable()->display(function ($id) {

            $filter = Filter::whereHas('values', function ($query) use ($id) {
                $query->where('id', $id);
            })->first();

            return __(MainEnum::LANG_GROUP . $filter->slug) . ': ' . __(MainEnum::LANG_GROUP . FilterValue::find($id)->slug);
        });;


        $grid->actions(function ($actions) {
            $actions->disableEdit();
            $actions->disableView();
        });
        $grid->disableCreateButton();

        return $grid;
    }

    public function index(Content $content, $product_id = null)
    {
        Admin::disablePjax();

        return $content
            ->title(__(MainEnum::LANG_GROUP . PivotTableEnum::PRODUCT_FILTER_VALUES))
            ->row(function (Row $row) use ($product_id) {

                $row->column(6, function (Column $column) use ($product_id) {
                    $column->append($this->grid($product_id));
                });

                $row->column(6, function (Column $column) use ($product_id) {
                    $data = $this->getVariables($product_id);
                    $column->append(view('admin::product.filters.add')->with(compact('data', 'product_id')));
                });
            });
    }

    protected function getVariables($product_id): array
    {
        $product = Product::where('id', $product_id)->with(['filters.values', 'values'])->first();
        $data['product'] = $product->toJson();
        return $data;
    }

    public function edit($item_id, Content $content, $id = null)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($item_id, $id)->edit($id));
    }

    public function create(Content $content, $item_id = null)
    {
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form($item_id));
    }

    public function createProductFilterValue(Request $request)
    {

        $user = new ProductFilterValue();
        $user->product_id = $request->product_id;
        $user->filter_value_id = $request->filter_value_id;
        $user->save();

        return redirect(route('product-filter-values.index', ['product_id' => $request->product_id]));

    }
}
