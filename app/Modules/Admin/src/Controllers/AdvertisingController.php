<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Advertising;
use App\Modules\Database\src\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class AdvertisingController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Advertising';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Advertising());
        $grid->column('id', __('ID'))->sortable();
        $grid->column(AdvertisingEnum::BANNER_TITLE, __(MainEnum::LANG_GROUP . AdvertisingEnum::BANNER_TITLE))->sortable();
        $grid->column(AdvertisingEnum::ADVERT_PAGE_DISCOUNT, __(MainEnum::LANG_GROUP . AdvertisingEnum::ADVERT_PAGE_DISCOUNT))->sortable();

        return $grid;
    }

    public function form($id = false)
    {
        $form = new Form(new Advertising());

        $form->display('id', __('ID'));
        $form->radio(MainEnum::VISIBLE, (MainEnum::LANG_GROUP . MainEnum::VISIBLE))->options([0 => 'false', 1 => 'true'])->default(1);
        $form->number(AdvertisingEnum::ADVERT_PAGE_DISCOUNT, (MainEnum::LANG_GROUP . AdvertisingEnum::ADVERT_PAGE_DISCOUNT));

//BANNER
        $form->embeds(AdvertisingEnum::BANNER_TITLE, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(AdvertisingEnum::BANNER_TITLE . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::BANNER_TITLE) . ' ' . $lang);
            }
        });
        $form->date(AdvertisingEnum::BANNER_EXPIRE_DATE, __(MainEnum::LANG_GROUP . AdvertisingEnum::BANNER_EXPIRE_DATE));
//
//        $form->embeds(AdvertisingEnum::BANNER_EXPIRE_DATE, function ($form) {
//            foreach (MainEnum::getLangs() as $lang) {
//            }
//        });
        $form->embeds(AdvertisingEnum::BANNER_DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->ckeditor(AdvertisingEnum::BANNER_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::BANNER_DESCRIPTION) . ' ' . $lang);
            }
        });
        $form->file(AdvertisingEnum::BANNER_IMAGE, __(MainEnum::LANG_GROUP . AdvertisingEnum::BANNER_IMAGE));
//ADVERT-PAGE
        $form->embeds(AdvertisingEnum::ADVERT_PAGE_TITLE, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(AdvertisingEnum::ADVERT_PAGE_TITLE . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::ADVERT_PAGE_TITLE) . ' ' . $lang);
            }
        });
        $form->embeds(AdvertisingEnum::ADVERT_PAGE_DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->ckeditor(AdvertisingEnum::ADVERT_PAGE_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::ADVERT_PAGE_DESCRIPTION) . ' ' . $lang);
            }
        });
//META
        $form->embeds(AdvertisingEnum::META_TAG, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(AdvertisingEnum::META_TAG . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::META_TAG) . ' ' . $lang);
            }
        });
        $form->embeds(AdvertisingEnum::META_DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(AdvertisingEnum::META_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::META_DESCRIPTION) . ' ' . $lang);
            }
        });

        if ($id) {
            $form->tools(function (Form\Tools $tools) use ($id) {
                $tools->disableView();
                $tools->add('<a class="btn btn-sm btn-info" href="' . route('page-block.index', ['advert_id' => $id]) . '"><i class="fa fa-product-hunt"></i>&nbsp;&nbsp;' . __('table.add-advert-block') . '</a>');
            });
        }
        $form->tools(function (Form\Tools $tools) {
            $tools->disableList();
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        return $form;
    }

    public function edit($id, Content $content)
    {
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($id)->edit($id));
    }

}
