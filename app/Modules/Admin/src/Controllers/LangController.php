<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Language;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class LangController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Lang translations';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() :Grid
    {
        $grid = new Grid(new Language());
        $grid->filter(function ($filter) {

            // Remove the default id filter
            $filter->disableIdFilter();

            // Add a column filter
            $arr = array_column(Language::select( 'group')->groupBy('group')->get()->toArray(), 'group');
            $filter->in('group', __('table.group'))->multipleSelect(
                (array_combine($arr, $arr))
            );

//            $filter->select()->optionns);
            $filter->ilike('text', __('table.text'));
            $filter->ilike('key', __('table.key'));


        });

        $grid->column('id', __('ID'))->sortable();
        $grid->column('group', __('group'))->sortable();
        $grid->column('key', __('key'))->sortable()->editable();
        $grid->column('text', __('text'))->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }


    public function form()
    {

//        Artisan::call("php artisan infyom:scaffold {$request['name']} --fieldsFile=public/Product.json");

        $form = new Form(new Language());
        $form->saved(function (Form $form) {
            Artisan::call("cache:clear");
        });
        $form->display('id', __('ID'));
        $form->text('group', __('group'));
        $form->text('key', __('key'));
        $form->embeds('text', function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text($lang);
            }
        });

        return $form;
    }

    public function setLocale(Request $request) {

        $admin = Admin::user();
        $langs = MainEnum::getLangs();
        if (!$request->lang || !isset($langs[$request->lang])) {
            return response([], 403);
        }
        $admin->lang = $request->lang;
        if (!$admin->save()) {
            return response([], 403);
        }

        return response(['status' => 'OK']);

    }

}
