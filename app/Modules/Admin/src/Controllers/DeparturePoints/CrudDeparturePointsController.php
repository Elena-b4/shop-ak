<?php

namespace App\Modules\Admin\src\Controllers\DeparturePoints;

use App\Modules\Database\src\Enums\DeparturePointEnum;
use App\Modules\Database\src\Models\DeparturePoint;
use Encore\Admin\Admin;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class CrudDeparturePointsController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new DeparturePoint());

        $grid->column('id', __('ID'))->sortable();
        $grid->column(DeparturePointEnum::NAME, __('Name'))->sortable();
        $grid->column(DeparturePointEnum::ADDRESS, __('Address'))->sortable();
        $grid->column(DeparturePointEnum::TIME_WORKS, __('Time works'))->sortable();
        $grid->column(DeparturePointEnum::DAY_WORKS, __('Day works'))->display(function ($days) {
            return json_decode($days[0], true);
        })->label([
            'Monday' => 'info',
            'Tuesday' => 'info',
            'Wednesday' => 'info',
            'Thursday' => 'info',
            'Friday' => 'info',
            'Saturday' => 'warning',
            'Sunday' => 'warning',
        ])->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    public function form()
    {
        $form = new Form(new DeparturePoint());

        $form->text(DeparturePointEnum::NAME, __('table.' . DeparturePointEnum::NAME));
        $form->text(DeparturePointEnum::ADDRESS, __('table.' . DeparturePointEnum::ADDRESS));
        $form->timeRange('created_at', 'updated_at', __('table.' . DeparturePointEnum::TIME_WORKS));
        $form->hidden(DeparturePointEnum::TIME_WORKS);

        if ($form->isCreating()) {
            $form->multipleSelect(DeparturePointEnum::DAY_WORKS, __('table.' . DeparturePointEnum::DAY_WORKS))
                ->options([
                    'Monday' => 'Monday',
                    'Tuesday' => 'Tuesday',
                    'Wednesday' => 'Wednesday',
                    'Thursday' => 'Thursday',
                    'Friday' => 'Friday',
                    'Saturday' => 'Saturday',
                    'Sunday' => 'Sunday'
                ]);
        } else {
            $form->multipleSelect(DeparturePointEnum::DAY_WORKS, __('table.' . DeparturePointEnum::DAY_WORKS))
                ->options(function ($days) {
                    Admin::script("$('select.day_works').val(".$days[0].");");
                    return [
                        'Monday' => 'Monday',
                        'Tuesday' => 'Tuesday',
                        'Wednesday' => 'Wednesday',
                        'Thursday' => 'Thursday',
                        'Friday' => 'Friday',
                        'Saturday' => 'Saturday',
                        'Sunday' => 'Sunday'
                    ];
                });
        }


        $form->saving(function (Form $form) {
            $form->day_works = json_encode(array_filter($form->day_works));
            $form->time_works = substr($form->created_at, 0, -3) . '-' . substr($form->updated_at, 0, -3);
        });

        return $form;
    }


}
