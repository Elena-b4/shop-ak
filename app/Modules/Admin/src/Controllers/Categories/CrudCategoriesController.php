<?php

namespace App\Modules\Admin\src\Controllers\Categories;

use App\Modules\Admin\src\Selectable\FilterSelectable;
use App\Modules\Common\src\Helpers\S3;
use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Category;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Encore\Admin\Widgets\Box;

use function PHPUnit\Framework\fileExists;

class CrudCategoriesController extends AdminController
{
    public function index(Content $content)
    {
        return $content
            ->title(__(MainEnum::LANG_GROUP . CategoryEnum::TABLE_NAME))
            ->row(function (Row $row) {

                $row->column(6, function (Column $column) {
                    $column->append(Category::tree(function ($tree) {
                        $tree->branch(function ($branch) {
                            $src = S3::getUrl($branch['icon']);
                            $logo = '';
                           if ($src) {
                               $logo = "<img src='$src' style='max-width:100px;max-height:100px' class='img'/>";
                           }
                            $string = ' ';
                            if ($branch['visible'] === true) {
                                $string= "<div class='btn btn-success'>Active</div>";
                            } else {
                                $string= "<a><div class='btn btn-danger'>Disable</div></a>";

                            }
                            return " $logo  {$branch['id']} - ".trans('category.'.$branch['slug']) . $string;
                        });
                    }));
                });

                $row->column(6, function (Column $column) {
                    $form = new \Encore\Admin\Widgets\Form();
                    $form->action(admin_url('categories'));

                    $form->select(MainEnum::PARENT_ID, __(MainEnum::LANG_GROUP .MainEnum::PARENT_ID))->options(Category::selectOptions());
                    $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP .MainEnum::SLUG))->rules('required');
                    $form->radio(MainEnum::VISIBLE,__(MainEnum::LANG_GROUP .MainEnum::VISIBLE))->options([0 => 'false', 1=> 'true'])->default(1);
                    $form->image(CategoryEnum::ICON, __(MainEnum::LANG_GROUP .CategoryEnum::ICON));
                    $form->belongsToMany('filters', FilterSelectable::class, __('table.filters'));

                    $form->hidden('_token')->default(csrf_token());

                    $column->append((new Box(__('admin.new'), $form))->style('success'));
                });
            });
    }


    public function form()
    {
        $form = new Form(new Category());
        $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP .MainEnum::SLUG))->rules('required');
        $form->select(MainEnum::PARENT_ID, __(MainEnum::LANG_GROUP .MainEnum::PARENT_ID))->options(Category::selectOptions());
        $form->radio(MainEnum::VISIBLE,__(MainEnum::LANG_GROUP .MainEnum::VISIBLE))->options([0 => 'false', 1=> 'true'])->default(1);
        $form->image(CategoryEnum::ICON, __('admin.'.CategoryEnum::ICON));
        $form->belongsToMany('filters', FilterSelectable::class, __('table.filters'));


        return $form;
    }



}
