<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use App\Modules\Database\src\Enums\MenuItemEnum;
use App\Modules\Database\src\Models\Menu;
use App\Modules\Database\src\Models\MenuItems;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Column;
use Encore\Admin\Layout\Content;
use Encore\Admin\Layout\Row;
use Illuminate\Http\Request;

class MenuController extends AdminController
{

    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Menu';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid(): Grid
    {
        $grid = new Grid(new Menu());
        $grid->model()->orderBy(MainEnum::SORT);

        $grid->column('id', __('ID'))->sortable();
        $grid->column(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG))->sortable();
        $grid->column(MenuEnum::URL, __(MainEnum::LANG_GROUP . MenuEnum::URL))->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }


    public function edit($id, Content $content)
    {


        return $content
            ->title(__(MainEnum::LANG_GROUP . CategoryEnum::TABLE_NAME))
            ->row(function (Row $row) use ($id) {

                $row->column(6, function (Column $column) use ($id){
                    $column->append(MenuItems::tree(function ($tree) use ($id){
                        $views= array(
                            'tree'   => 'admin::menu.tree',
                            'branch' => 'admin::tree.branch');
                        $tree->setView($views);
                        $tree->query(function ($model) use ($id) {
                            return $model->where(MenuEnum::MENU_ID, $id);
                        });
                        $tree->branch(function ($branch) {
                            $src = asset('storage/' . $branch['slug']);

                            return "   {$branch['id']} - " . __(MainEnum::LANG_GROUP . $branch['slug']) ;
                        });
                    }));
                });

                $row->column(6, function (Column $column) use ($id) {
                    $column->append($this->form()->edit($id));

                });
            });
    }

    public function form()
    {
        $form = new Form(new Menu());

        $form->display('id', __('ID'));

        $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG))->rules('required');
        $form->number(MainEnum::SORT, __(MainEnum::LANG_GROUP . MainEnum::SORT))->default(Menu::count() + 1);
        $form->radio(MainEnum::VISIBLE, MainEnum::LANG_GROUP . MainEnum::VISIBLE)->options([0 => 'No', 1 => 'Yes'])->default(0);
        $form->text(MenuItemEnum::URL, __(MainEnum::LANG_GROUP . MenuItemEnum::URL));

        $form->hasMany('items', __(MainEnum::LANG_GROUP . MainEnum::VALUES), function (Form\NestedForm $form) {
//                $form->select(MainEnum::PARENT_ID, __(MainEnum::LANG_GROUP .MainEnum::PARENT_ID))->options(MenuItems::selectOptions());
            $form->text(MenuItemEnum::URL, __(MainEnum::LANG_GROUP . MenuItemEnum::URL));
            $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG));
        });


        return $form;
    }


    public function tree(Request $request)
    {
        $tree = json_decode($request->_order, true);

        if (json_last_error() != JSON_ERROR_NONE) {
            throw new \InvalidArgumentException(json_last_error_msg());
        }

        MenuItems::saveOrder($tree);

        return true;
    }

}
