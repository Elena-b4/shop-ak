<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\Advertising;
use App\Modules\Database\src\Models\AdvertPageBlock;
use App\Modules\Database\src\Models\AdvertProduct;
use App\Modules\Database\src\Models\Item;
use App\Modules\Database\src\Models\Product;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Support\Facades\Request;
use phpDocumentor\Reflection\Types\Integer;

class AdvertisingPageBlockController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Advertising page block';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid($advertId): Grid
    {
        $grid = new Grid(new AdvertPageBlock());
        $grid->model()->where(AdvertisingEnum::ADVERT_PAGE_ID, $advertId)->orderBy('id');

        $grid->column('id', __('ID'))->sortable();
        $grid->column(AdvertisingEnum::ADVERT_PAGE_BLOCK_SLUG, __(MainEnum::LANG_GROUP . AdvertisingEnum::ADVERT_PAGE_BLOCK_SLUG))->sortable();
        $grid->column(AdvertisingEnum::DESCRIPTION, __(MainEnum::LANG_GROUP . AdvertisingEnum::DESCRIPTION))->sortable();
        $grid->disableExport();
        $grid->disableRowSelector();
        $grid->disableColumnSelector();
        $grid->disableFilter();
        return $grid;
    }

    public function form($advertId = null, $id = false)
    {
        $form = new Form(new AdvertPageBlock());
        $form->hidden(AdvertisingEnum::ADVERT_PAGE_ID)->value($advertId);

        $products = Product::all()->mapWithKeys(function ($value, $key) {
            $lang = app()->getLocale();
            foreach ($value->title as $k => $title) {
                $k = substr($k, 5, 2);
                if ($lang == $k) {
                    $key = $value->id;
                    $value = $title;
                }
            }
            return [$key => $value];
        });
        $form->text(MainEnum::SLUG);

        $form->multipleSelect('products', 'Products')->options($products->all());

        $form->embeds(AdvertisingEnum::DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(AdvertisingEnum::DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . AdvertisingEnum::DESCRIPTION) . ' ' . $lang);
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableList();
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        return $form;
    }


    public function edit($advertId, Content $content, $id = null)
    {
        $pageBlock = AdvertPageBlock::find($id);
        if (!$pageBlock) {
            abort(404);
        }
        return $content
            ->title($this->title())
            ->description($this->description['edit'] ?? trans('admin.edit'))
            ->body($this->form($advertId, $id)->edit($id));
    }

    public function create(Content $content, $advertId = null)
    {
        $advert = Advertising::find($advertId);
        if (!$advert) {
            abort(404);
        }
        return $content
            ->title($this->title())
            ->description($this->description['create'] ?? trans('admin.create'))
            ->body($this->form($advertId));
    }

    public function index(Content $content, $advertId = null)
    {
        $advert = Advertising::find($advertId);
        if (!$advert) {
            abort(404);
        }
        return $content
            ->title($this->title())
            ->description($this->description['index'] ?? trans('admin.list'))
            ->body($this->grid($advertId));
    }

    public function update($advertId, $id = null)
    {
        return $this->form()->update($id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($advertId, $id = null)
    {
        return $this->form()->destroy($id);
    }
}
