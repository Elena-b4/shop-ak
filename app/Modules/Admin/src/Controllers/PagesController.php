<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Admin\src\Helpers\MetaFormHelper;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PageEnum;
use App\Modules\Database\src\Models\Filters;
use App\Modules\Database\src\Models\Page;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\Table;
use \Cviebrock\EloquentSluggable\Services\SlugService;


class PagesController extends AdminController
{
    protected $title = 'Pages';

    protected function grid(): Grid
    {
        $grid = new Grid(new Page());
        $grid->actions(function ($actions) {
            $actions->disableView();
        });
        $grid->disableExport();
        $grid->disableFilter();
        $grid->column('id', __('ID'))->sortable();
        $grid->column(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG))->sortable();
        $grid->column(MainEnum::TITLE, __(MainEnum::LANG_GROUP . MainEnum::TITLE))->display(function () {
            $lang = app()->getLocale();
            return $this->title[MainEnum::TITLE . $lang];
        });
        $grid->column(PageEnum::IMAGE, __(PageEnum::LANG_GROUP . PageEnum::IMAGE))->image();
        return $grid;
    }

    public function form()
    {
        $form = new Form(new Page());

        $form->footer(function ($footer) {

            $footer->disableViewCheck();

            $footer->disableEditingCheck();

            $footer->disableCreatingCheck();
        });

        $form->tools(function (Form\Tools $tools) {

            $tools->disableList();

            $tools->disableView();

        });
        $form->display('id', __('ID'));
        $form->text(MainEnum::SLUG, __(MainEnum::LANG_GROUP . MainEnum::SLUG));
        $form->image(PageEnum::IMAGE, __(PageEnum::LANG_GROUP . PageEnum::IMAGE));
        $form->embeds(MainEnum::TITLE, __(MainEnum::LANG_GROUP . MainEnum::TITLE), function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(MainEnum::TITLE . $lang, __(MainEnum::LANG_GROUP . MainEnum::TITLE) . ' ' . $lang);
            }
        });
        $form->embeds(PageEnum::TEXT, __(PageEnum::LANG_GROUP . PageEnum::TEXT), function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->ckeditor(PageEnum::TEXT . $lang, __(PageEnum::LANG_GROUP . PageEnum::TEXT) . ' ' . $lang);
            }
        });

        MetaFormHelper::getMetaForm($form);

        $form->saving(function (Form $form) {
            if (!$form->input(MainEnum::SLUG)) {
                $from = $form->input(MainEnum::TITLE)[MainEnum::TITLE . config('app.locale')];
                $value = SlugService::createSlug(Page::class, MainEnum::SLUG, $from);
                $form->input(MainEnum::SLUG, $value);
            }
        });

        return $form;
    }


}
