<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Models\CompanyInfo;
use App\Modules\Database\src\Enums\CompanyInfoEnum;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class CompanyInfoController extends AdminController
{
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Company info';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() :Grid
    {
        $grid = new Grid(new CompanyInfo());

        $grid->column('id', __('ID'))->sortable();
        $grid->column(CompanyInfoEnum::PHONES, __('Phones'))->sortable();
        $grid->column(CompanyInfoEnum::EMAILS, __('Emails'))->sortable();
        $grid->column(CompanyInfoEnum::NAME, __('Names'))->sortable();
        $grid->column(CompanyInfoEnum::ADDRESS, __('Address'))->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    public function form()
    {
        $form = new Form(new CompanyInfo());

        $form->display('id', __('ID'));
        $form->text(CompanyInfoEnum::PHONES, __(CompanyInfoEnum::PHONES));
        $form->text(CompanyInfoEnum::EMAILS, __(CompanyInfoEnum::EMAILS));

        $form->embeds(CompanyInfoEnum::DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(CompanyInfoEnum::DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . CompanyInfoEnum::DESCRIPTION) . ' ' . $lang);
            }
        });

        $form->embeds(CompanyInfoEnum::ADDRESS, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(CompanyInfoEnum::ADDRESS . $lang, __(MainEnum::LANG_GROUP . CompanyInfoEnum::ADDRESS) . ' ' . $lang);
            }
        });

        $form->embeds(CompanyInfoEnum::NAME, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(CompanyInfoEnum::NAME . $lang, __(MainEnum::LANG_GROUP . CompanyInfoEnum::NAME) . ' ' . $lang);
            }
        });

        $form->embeds(CompanyInfoEnum::META_DESCRIPTION, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(CompanyInfoEnum::META_DESCRIPTION . $lang, __(MainEnum::LANG_GROUP . CompanyInfoEnum::META_DESCRIPTION) . ' ' . $lang);
            }
        });

        $form->embeds(CompanyInfoEnum::META_TAG, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(CompanyInfoEnum::META_TAG . $lang, __(MainEnum::LANG_GROUP . CompanyInfoEnum::META_TAG) . ' ' . $lang);
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableList();
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        return $form;
    }


}
