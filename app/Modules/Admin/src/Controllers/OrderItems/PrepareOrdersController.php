<?php

namespace App\Modules\Admin\src\Controllers\OrderItems;

use App\Modules\Database\src\Enums\MainEnum;
use Encore\Admin\Layout\Content;

class PrepareOrdersController extends BaseClassController
{


    public function list(Content $content)
    {
//        $this->orderRepository->getWishlistItems();
        return $content
            ->title(__(MainEnum::LANG_GROUP . 'prepare orders'))
            ->description()
            ->body(view('admin::orderItems.prepareOrders.list'));
    }

    public function items()
    {
        $prepareItem = $this->orderRepository->getPrepareItems();
        return response()->json($prepareItem);

    }

}
