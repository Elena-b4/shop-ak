<?php

namespace App\Modules\Admin\src\Controllers\OrderItems;

use App\Http\Controllers\Controller;
use App\Modules\Admin\src\Repositories\Contracts\OrderRepositoryInterface;

abstract class BaseClassController extends Controller
{

    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

}
