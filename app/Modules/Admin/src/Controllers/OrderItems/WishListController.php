<?php

namespace App\Modules\Admin\src\Controllers\OrderItems;

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ShopOrderEnum;
use App\Modules\Database\src\Models\Item;
use App\Modules\Database\src\Models\ShopOrder;
use App\Modules\Database\src\Models\ShopOrderItems;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;

class WishListController extends BaseClassController
{


    public function list(Content $content)
    {
//        $this->orderRepository->getWishlistItems();
        return $content
            ->title(__(MainEnum::LANG_GROUP . 'wishlist'))
            ->description()
            ->body(view('admin::orderItems.wishlist.list'));
    }

    //
    public function items()
    {
        $items = $this->orderRepository->getWishListItems();


        return response()->json($items);
    }

    public function getProductWithItems(Request $request)
    {
        $products = $this->orderRepository->searchProductByProductNameOrVendorCode($request->search);
        return response()->json($products);
    }

    public function updateItemNeedOrder($item_id)
    {
        $item = $this->orderRepository->updateItemNeedOrder($item_id);

        return response($item);
    }


    public function addItemToPrepareOrder(Request $request)
    {
        (int)$item_id = $request->item_id;

        $prepareOrder = ShopOrder::firstOrCreate([ShopOrderEnum::STATUS => ShopOrderEnum::STATUS_PREPARE_ORDER]);

        $itemNeedOrder = Item::find($item_id)->need_order;
        if (!ShopOrderItems::where(ItemEnum::ITEM_ID, $item_id)->first() && $itemNeedOrder) {
            ShopOrderItems::create([ItemEnum::ITEM_ID => $item_id, ShopOrderEnum::SHOP_ORDER_ID => $prepareOrder->id]);
        } else {
            abort(404);
        }

    }

}
