<?php

namespace App\Modules\Admin\src\Controllers;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\SocialEnum;
use App\Modules\Database\src\Models\Socials;
use Encore\Admin\Controllers\AdminController;
use Encore\Admin\Form;
use Encore\Admin\Grid;

class SocialsController extends AdminController
{

    public function store()
    {
//        dd(request()->all());
        return $this->form()->store();
    }
    /**
     * Title for current resource.
     *
     * @var string
     */
    protected $title = 'Socials';

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid() :Grid
    {
        $grid = new Grid(new Socials());

        $grid->column('id', __('ID'))->sortable();
        $grid->column(SocialEnum::ICON, __(SocialEnum::ICON))->sortable();
//        $grid->column(SocialEnum::NAME, __(SocialEnum::NAME))->sortable();
        $grid->column(SocialEnum::URL, __(SocialEnum::URL))->sortable();
        $grid->column(MainEnum::SORT, __(MainEnum::SORT))->sortable();
        $grid->column('created_at', __('Created at'));
        $grid->column('updated_at', __('Updated at'));

        return $grid;
    }

    public function form()
    {
        $form = new Form(new Socials());

        $form->display('id', __('ID'));
        $form->text(SocialEnum::ICON, __(SocialEnum::ICON));
        $form->text(SocialEnum::URL, __(SocialEnum::URL));
        $form->text(MainEnum::SORT, __(MainEnum::SORT));

        $form->embeds(SocialEnum::NAME, function ($form) {
            foreach (MainEnum::getLangs() as $lang) {
                $form->text(SocialEnum::NAME . $lang, __(MainEnum::LANG_GROUP . SocialEnum::NAME) . ' ' . $lang);
            }
        });

        $form->tools(function (Form\Tools $tools) {
            $tools->disableList();
            $tools->disableDelete();
            $tools->disableView();
        });
        $form->footer(function ($footer) {
            $footer->disableReset();
            $footer->disableViewCheck();
            $footer->disableEditingCheck();
            $footer->disableCreatingCheck();
        });

        return $form;
    }

}
