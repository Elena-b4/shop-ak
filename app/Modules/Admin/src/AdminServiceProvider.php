<?php


namespace App\Modules\Admin\src;




use App\Modules\Admin\src\Repositories\Contracts\OrderRepositoryInterface;
use App\Modules\Admin\src\Repositories\OrderRepository;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AdminServiceProvider extends ServiceProvider
{

    public $bindings = [
        OrderRepositoryInterface::class => OrderRepository::class,
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {

    }


    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerViews();

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        Route::middleware([])->group(__DIR__ . '/../routes/routes.php');
    }


    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews(){

        $this->loadViewsFrom(
            __DIR__ . '/../resources/views', 'admin'
        );
    }


}
