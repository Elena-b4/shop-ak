<?php

namespace App\Modules\Admin\src\Grid;

use Encore\Admin\Actions\RowAction;
use Illuminate\Database\Eloquent\Model;

class ProductRow extends RowAction
{
    public $name;

    public function __construct()
    {
        $this->name = __('table.items');
    }

    public function handle(Model $model)
    {
        return redirect()->route('items.index', ['product_id' => $model->product_id]);
    }

}
