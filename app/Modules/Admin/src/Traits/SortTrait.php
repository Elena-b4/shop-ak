<?php


namespace App\Modules\Admin\src\Traits;

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\MainEnum;

trait SortTrait
{
    public $namespace = 'App\Modules\Database\src\Models\\';

    public function sort($form, $model)
    {
        $model = $this->namespace . $model;
        $sortValuesArray = range(0, 0);
        $lastProductsSortValue = $model::all()->sortBy(MainEnum::SORT)->last();
        if ($lastProductsSortValue) {
            $sortValuesArray = range(0, ++$lastProductsSortValue->sort);
            $form->submitted(function () use ($lastProductsSortValue, $model) {
                if (request('sort') !== (++$lastProductsSortValue->sort)) {
                    $nextProducts = $model::where('sort', '>=', request('sort'))->get();
                    foreach ($nextProducts as $nextProduct) {
                        $nextProduct->sort = $nextProduct->sort + 1;
                        $nextProduct->save();
                    }
                }
            });
        }

        $form->select(MainEnum::SORT, __(MainEnum::LANG_GROUP . MainEnum::SORT))
            ->options($model::all(['id', MainEnum::SORT])->pluck(MainEnum::SORT, 'id'))->required()->default(0)
            ->options($sortValuesArray);
    }

    public function sortFiles($form, $model, $entity, $entityId)
    {
        $model = $this->namespace . $model;
        $sortValuesArray = range(0, 0);
        $lastProductsSortValue = $model::where([FileEnum::ENTITY => $entity, FileEnum::ENTITY_ID => $entityId])->get()
            ->sortBy(MainEnum::SORT)->last();
        if ($lastProductsSortValue) {
            $sortValuesArray = range(0, ++$lastProductsSortValue->sort);
            $form->submitted(function () use ($lastProductsSortValue, $model, $entityId, $entity) {
                if (request('sort') !== (++$lastProductsSortValue->sort)) {
                    $nextProducts = $model::where([FileEnum::ENTITY => $entity, FileEnum::ENTITY_ID => $entityId])
                    ->where('sort', '>=', request('sort'))->get();
                    foreach ($nextProducts as $nextProduct) {
                        $nextProduct->sort = $nextProduct->sort + 1;
                        $nextProduct->save();
                    }
                }
            });
        }

        $form->select(MainEnum::SORT, __(MainEnum::LANG_GROUP . MainEnum::SORT))
            ->options($model::all(['id', MainEnum::SORT])->pluck(MainEnum::SORT, 'id'))->required()->default(0)
            ->options($sortValuesArray);
    }
}
