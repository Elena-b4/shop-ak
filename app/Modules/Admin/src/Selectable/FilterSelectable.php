<?php

namespace App\Modules\Admin\src\Selectable;

use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Models\Filter as FilterModel;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Grid\Selectable;

class FilterSelectable extends Selectable
{
    public $model = FilterModel::class;

    public function make()
    {
        $this->column('id');
        $this->column(MainEnum::SLUG, __(FilterEnum::LANG_GROUP . MainEnum::SLUG));
        $this->column(MainEnum::SORT, __(FilterEnum::LANG_GROUP . MainEnum::SORT));
        $this->column(MainEnum::VISIBLE, __(FilterEnum::LANG_GROUP . MainEnum::VISIBLE));
        $this->column('created_at');

        $this->filter(function (Filter $filter) {
            $filter->like(MainEnum::SLUG);
        });
    }
}
