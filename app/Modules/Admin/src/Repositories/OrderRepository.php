<?php

namespace App\Modules\Admin\src\Repositories;

use App\Modules\Admin\src\Repositories\Contracts\OrderRepositoryInterface;
use App\Modules\Api\src\Models\ItemApi;
use App\Modules\Api\src\Models\ProductApi;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ShopOrderEnum;
use App\Modules\Database\src\Models\Item;
use App\Modules\Database\src\Models\ShopOrder;

class OrderRepository implements OrderRepositoryInterface
{
    public function searchProductByProductNameOrVendorCode($queryString)
    {
        $queryString = '%' . $queryString . '%';
        return ProductApi::
        where(function ($q) use ($queryString) {
            $q->whereHas('names', function ($query) use ($queryString) {
                $query->where(ProductEnum::NAME, 'like', $queryString);
            });
        })
//            ->orWhere(function ($q) use ($queryString) {
//                $q->whereHas('items', function ($query) use ($queryString) {
//                    $query->where(ItemEnum::VENDOR_CODE, 'like', $queryString);
////                $query->orWhere(ItemEnum::BAR_CODE, $queryString);
//                });
//            })
            ->with(['images', 'items'])->paginate(10);
    }

    public function updateItemNeedOrder($item_id)
    {
        $itemInPrepareOrder = ShopOrder::where([ShopOrderEnum::STATUS => ShopOrderEnum::STATUS_PREPARE_ORDER])
            ->whereHas('items', function ($query) use ($item_id) {
                $query->where(ItemEnum::ITEM_ID, $item_id);
            })->exists();
        if ($itemInPrepareOrder) {
            return response()->json(['error' => 'item exists in prepare order']);
        }
        $item = Item::find($item_id);
        if ($item->need_order) {
            $item = Item::where('id', $item_id)->update([ItemEnum::NEED_ORDER => false]);
            return $item;
        }
        $item = Item::where('id', $item_id)->update([ItemEnum::NEED_ORDER => true]);

        return $item;
    }

    public function getWishListItems()
    {
        //получаем ордер, готовый к заказу и получаем их ids
        $itemsInPrepareOrder = ShopOrder::where([ShopOrderEnum::STATUS => ShopOrderEnum::STATUS_PREPARE_ORDER])
            ->with('items')->first();
        $itemIdsInPrepareOrder = [];
        if ($itemsInPrepareOrder && $itemsInPrepareOrder->items) {
            foreach ($itemsInPrepareOrder->items as $item) {
                $itemIdsInPrepareOrder[] = $item->item_id;
            }
        }
        $items = ItemApi::where(ItemEnum::NEED_ORDER, true)->with(['product', 'points', 'images']);

        if (!empty($itemIdsInPrepareOrder)) {
            $items->whereNotIn('id', $itemIdsInPrepareOrder);
        }

        $items = $items->paginate(10);
        return $items->each(function ($i, $k) {
            $i->makeVisible(['id']);
        });
    }

    public function getPrepareItems()
    {
        $items = ItemApi::whereHas('shopOrder', function ($query) {
            $query->where(ShopOrderEnum::STATUS, ShopOrderEnum::STATUS_PREPARE_ORDER);
        })->with(['product', 'images', 'points'])->paginate(10);
        return $items->each(function ($i, $k) {
            $i->makeVisible(['id']);
        });
    }
}
