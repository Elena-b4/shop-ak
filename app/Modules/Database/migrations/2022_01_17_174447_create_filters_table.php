<?php

use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FilterEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::SLUG);
            $table->integer(MainEnum::SORT)->default(0);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(FilterEnum::TABLE_NAME);
    }
}
