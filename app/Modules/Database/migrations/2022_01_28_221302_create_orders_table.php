<?php

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\OrderEnum;
use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OrderEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId(UserEnum::USER_ID)->references('id')->on(UserEnum::TABLE_NAME);
            $table->foreignId(OrderEnum::ADMIN_USER_ID)->references('id')->on(UserEnum::TABLE_NAME);
            $table->integer(ItemEnum::ITEM_ID);
            $table->integer(OrderEnum::COUNT);
            $table->integer(OrderEnum::DELIVERY_ADDRESS);
            $table->integer(OrderEnum::DEPARTURE_POINT_ID);
            $table->string(OrderEnum::ORDER_ID);
            $table->integer(OrderEnum::PAYMENT_TYPE);
            $table->integer(OrderEnum::PAYMENT_STATUS);
            $table->integer(OrderEnum::PAYMENT_DATE);
            $table->integer(OrderEnum::DATE_DELIVERY);
            $table->integer(OrderEnum::DISCOUNT_POINTS_AWARDED);
            $table->integer(OrderEnum::STATUS);
            $table->integer(OrderEnum::DELETE_BY_USER);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(OrderEnum::TABLE_NAME);
    }
}
