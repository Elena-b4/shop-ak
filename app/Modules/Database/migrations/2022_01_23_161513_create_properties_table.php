<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PropertyEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PropertyEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::SLUG);
            $table->string(MainEnum::VISIBLE)->default(0);
            $table->string(MainEnum::SORT)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PropertyEnum::TABLE_NAME);
    }
}
