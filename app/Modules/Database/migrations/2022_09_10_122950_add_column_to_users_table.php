<?php

use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(UserEnum::TABLE_NAME, function (Blueprint $table) {
            $table->string(UserEnum::EMAIL)->nullable()->change();
            $table->timestamp(UserEnum::PHONE_VERIFIED_AT)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(UserEnum::TABLE_NAME, function (Blueprint $table) {
            $table->string(UserEnum::EMAIL)->change();
            $table->dropColumn(UserEnum::PHONE_VERIFIED_AT);
        });
    }
}
