<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MenuEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::SLUG);
            $table->string(MenuEnum::URL)->nullable()->default(null);
            $table->integer(MainEnum::SORT)->default(0);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(MenuEnum::TABLE_NAME);
    }
}
