<?php

use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CategoryEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::SLUG);
            $table->integer(MainEnum::PARENT_ID)->default(0);
            $table->string(CategoryEnum::ICON)->nullable();
            $table->integer(MainEnum::SORT)->default(0);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CategoryEnum::TABLE_NAME);
    }
}
