<?php

use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\FilterEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_filters', function (Blueprint $table) {
            $table->id();
            $table->foreignId(CategoryEnum::CATEGORY_ID)->references('id')->on(CategoryEnum::TABLE_NAME);
            $table->foreignId(FilterEnum::FILTER_ID)->references('id')->on(FilterEnum::TABLE_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_filters');
    }
}
