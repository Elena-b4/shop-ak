<?php

use App\Modules\Database\src\Enums\DeparturePointEnum;
use App\Modules\Database\src\Enums\ItemsCountEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsCountTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ItemsCountEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ItemEnum::ITEM_ID)->references('id')->on(ItemEnum::TABLE_NAME);
            $table->foreignId(DeparturePointEnum::DEPARTURE_POINT_ID)->references('id')->on(DeparturePointEnum::TABLE_NAME);
            $table->integer(ItemsCountEnum::COUNT)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ItemsCountEnum::TABLE_NAME);
    }
}
