<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->text(ProductEnum::SHORT_DESCRIPTION);
            $table->text(MainEnum::TITLE);
            $table->float(ProductEnum::PRICE);
            $table->text(ProductEnum::META_DESCRIPTION);
            $table->text(ProductEnum::DESCRIPTION);
            $table->string(MainEnum::SLUG);
            $table->boolean(ProductEnum::HIT)->default(0);
            $table->boolean(ProductEnum::NEW)->default(0);
            $table->integer(MainEnum::SORT)->default(0);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->text(ProductEnum::META_TAG);
            $table->integer(ProductEnum::BRAND_ID)->default(null)->nullable();
            $table->uuid(ProductEnum::UUID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ProductEnum::TABLE_NAME);
    }
}
