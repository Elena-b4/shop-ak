<?php

use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(UserEnum::NAME);
            $table->string(UserEnum::PHONE);
            $table->string(UserEnum::EMAIL);
            $table->timestamp(UserEnum::EMAIL_VERIFIED_AT)->nullable();
            $table->string(UserEnum::PASSWORD);
            $table->integer(UserEnum::DISCOUNT_POINTS)->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserEnum::TABLE_NAME);
    }
}
