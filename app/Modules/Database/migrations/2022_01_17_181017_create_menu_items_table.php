<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use App\Modules\Database\src\Enums\MenuItemEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(MenuItemEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::SLUG);
            $table->string(MenuItemEnum::URL);
            $table->integer(MainEnum::SORT)->default(0);
            $table->integer(MainEnum::PARENT_ID)->default(0);
            $table->integer(MenuEnum::MENU_ID);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(MenuItemEnum::TABLE_NAME);
    }
}
