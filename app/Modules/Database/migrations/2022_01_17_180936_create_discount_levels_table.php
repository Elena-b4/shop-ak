<?php

use App\Modules\Database\src\Enums\DiscountLevelEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DiscountLevelEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::TITLE);
            $table->integer(DiscountLevelEnum::DISCOUNT_AMOUNT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DiscountLevelEnum::TABLE_NAME);
    }
}
