<?php

use App\Modules\Database\src\Enums\CartEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCartTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CartEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId(UserEnum::USER_ID)->references('id')->on(UserEnum::TABLE_NAME);
            $table->integer(ItemEnum::ITEM_ID);
            $table->integer(CartEnum::COUNT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CartEnum::TABLE_NAME);
    }
}
