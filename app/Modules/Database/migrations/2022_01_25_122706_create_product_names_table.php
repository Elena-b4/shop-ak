<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductEnum::TABLE_NAME_NAMES, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::LANG);
            $table->string(ProductEnum::NAME);
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->timestamps();

        });

        Schema::table(ProductEnum::TABLE_NAME_NAMES, function (Blueprint $table) {
            $table->fullText(ProductEnum::NAME);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_names');
    }
}
