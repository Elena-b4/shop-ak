<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\SocialEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSocialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SocialEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->jsonb(SocialEnum::NAME);
            $table->string(SocialEnum::ICON);
            $table->string(SocialEnum::URL);
            $table->integer(MainEnum::SORT)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(SocialEnum::TABLE_NAME);
    }
}
