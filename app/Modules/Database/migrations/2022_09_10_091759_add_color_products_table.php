<?php

use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint as BlueprintAlias;
use Illuminate\Support\Facades\Schema;

class AddColorProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ProductEnum::TABLE_NAME, function (BlueprintAlias $table) {
            $table->string(ProductEnum::COLOR)->default(null)->nullable();
            $table->string(ProductEnum::NAME)->default(null)->nullable();
            $table->string(ProductEnum::VENDOR_CODE)->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(ProductEnum::TABLE_NAME, function (BlueprintAlias $table) {
            $table->dropColumn(ProductEnum::COLOR);
            $table->dropColumn(ProductEnum::NAME);
            $table->dropColumn(ProductEnum::VENDOR_CODE);
        });
    }
}
