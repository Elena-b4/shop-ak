<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PageEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create(PageEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(PageEnum::IMAGE)->nullable();
            $table->longText(PageEnum::TEXT);
            $table->longText(MainEnum::TITLE);
            $table->longText(MainEnum::META_DESCRIPTION)->nullable();
            $table->longText(MainEnum::META_TAG)->nullable();
            $table->string(MainEnum::SLUG)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists(PageEnum::TABLE_NAME);
    }
};
