<?php

use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FilterValueEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(MainEnum::SLUG);
            $table->integer(MainEnum::SORT)->default(0);
            $table->integer(FilterEnum::FILTER_ID)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(FilterValueEnum::TABLE_NAME);
    }
}
