<?php

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\FilterValueEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisingFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising_filters', function (Blueprint $table) {
            $table->id();
            $table->foreignId(FilterValueEnum::FILTER_VALUE_ID)->references('id')->on(FilterValueEnum::TABLE_NAME);
            $table->foreignId(AdvertisingEnum::ADVERTISING_ID)->references('id')->on(AdvertisingEnum::TABLE_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising_filters');
    }
}
