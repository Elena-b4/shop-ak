<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ReviewEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReviewEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(ReviewEnum::USER_NAME);
            $table->string(ReviewEnum::USER_EMAIL);
            $table->integer(ReviewEnum::RATING)->nullable();
            $table->text(ReviewEnum::CONTENT);
            $table->boolean(ReviewEnum::APPROVED)->default(0);
            $table->integer(MainEnum::PARENT_ID)->default(0);
            $table->string(ReviewEnum::PRODUCT_UUID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ReviewEnum::TABLE_NAME);
    }
}
