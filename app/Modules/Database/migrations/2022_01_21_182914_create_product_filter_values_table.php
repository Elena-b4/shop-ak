<?php

use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductFilterValuesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PivotTableEnum::PRODUCT_FILTER_VALUES, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->foreignId(FilterValueEnum::FILTER_VALUE_ID)->references('id')->on(FilterValueEnum::TABLE_NAME);
            $table->integer(MainEnum::SORT)->default(1);
            $table->integer(MainEnum::PARENT_ID)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PivotTableEnum::PRODUCT_FILTER_VALUES);
    }
}
