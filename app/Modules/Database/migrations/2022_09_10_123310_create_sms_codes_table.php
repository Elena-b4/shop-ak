<?php

use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(UserEnum::TABLE_NAME_SMS_CODES, function (Blueprint $table) {
            $table->id();
            $table->integer(UserEnum::CODE);
            $table->string(UserEnum::PHONE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(UserEnum::TABLE_NAME_SMS_CODES);
    }
}
