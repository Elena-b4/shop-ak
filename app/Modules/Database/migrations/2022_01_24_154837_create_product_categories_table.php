<?php

use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PivotTableEnum::PRODUCT_CATEGORIES, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->foreignId(CategoryEnum::CATEGORY_ID)->references('id')->on(CategoryEnum::TABLE_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PivotTableEnum::PRODUCT_CATEGORIES);
    }
}
