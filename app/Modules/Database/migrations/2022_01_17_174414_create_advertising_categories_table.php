<?php

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisingCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertising_categories', function (Blueprint $table) {
            $table->id();
            $table->foreignId(AdvertisingEnum::ADVERTISING_ID)->references('id')->on(AdvertisingEnum::TABLE_NAME);
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertising_categories');
    }
}
