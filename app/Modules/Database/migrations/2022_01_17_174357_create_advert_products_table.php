<?php

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AdvertisingEnum::ADVERT_PAGE_PRODUCT_TABLE, function (Blueprint $table) {
            $table->id();
            $table->foreignId(AdvertisingEnum::ADVERT_PAGE_BLOCK_ID)->references('id')->on(AdvertisingEnum::ADVERT_PAGE_BLOCK_TABLE)->onDelete('cascade');
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AdvertisingEnum::ADVERT_PAGE_PRODUCT_TABLE);
    }
}
