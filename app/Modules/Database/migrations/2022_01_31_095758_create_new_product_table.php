<?php

use App\Modules\Database\src\Enums\NewProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(NewProductEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->float(NewProductEnum::PRICE);
            $table->integer(NewProductEnum::COUNT);
            $table->integer(NewProductEnum::DESCRIPTION);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(NewProductEnum::TABLE_NAME);
    }
}
