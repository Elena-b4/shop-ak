<?php

use App\Modules\Database\src\Enums\DeparturePointEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeparturePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(DeparturePointEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(DeparturePointEnum::NAME);
            $table->string(DeparturePointEnum::ADDRESS);
            $table->string(DeparturePointEnum::TIME_WORKS);
            $table->string(DeparturePointEnum::DAY_WORKS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(DeparturePointEnum::TABLE_NAME);
    }
}
