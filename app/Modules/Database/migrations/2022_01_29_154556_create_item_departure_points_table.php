<?php

use App\Modules\Database\src\Enums\DeparturePointEnum as DeparturePointEnum;
use App\Modules\Database\src\Enums\ItemEnum as ItemEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemDeparturePointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PivotTableEnum::ITEM_DEPARTURE_POINT, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ItemEnum::ITEM_ID)->references('id')->on(ItemEnum::TABLE_NAME);
            $table->foreignId(DeparturePointEnum::DEPARTURE_POINT_ID)->references('id')->on(DeparturePointEnum::TABLE_NAME);
            $table->integer(PivotTableEnum::COUNT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PivotTableEnum::ITEM_DEPARTURE_POINT);
    }
}
