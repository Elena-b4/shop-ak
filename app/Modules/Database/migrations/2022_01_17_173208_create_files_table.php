<?php

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FileEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(FileEnum::NAME);
            $table->integer(MainEnum::SORT)->default(0);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->string(FileEnum::TYPE);
            $table->string(FileEnum::ENTITY);
            $table->integer(FileEnum::ENTITY_ID)->nullable();
            $table->integer(MainEnum::PARENT_ID)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(FileEnum::TABLE_NAME);
    }
}
