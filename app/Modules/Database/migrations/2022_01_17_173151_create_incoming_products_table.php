<?php

use App\Modules\Database\src\Enums\DeparturePointEnum;
use App\Modules\Database\src\Enums\IncomingProductEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIncomingProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(IncomingProductEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->integer(IncomingProductEnum::COUNT);
            $table->foreignId(IncomingProductEnum::ADMIN_USER_ID)->references('id')->on(IncomingProductEnum::ADMIN_USERS);
            $table->foreignId(DeparturePointEnum::DEPARTURE_POINT_ID)->references('id')->on(DeparturePointEnum::TABLE_NAME);
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(IncomingProductEnum::TABLE_NAME);
    }
}
