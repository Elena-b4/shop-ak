<?php

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSizeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(ItemEnum::TABLE_NAME, function (Blueprint $table) {
            $table->string(ItemEnum::SIZE)->default(null)->nullable();
            $table->integer(ItemEnum::QUANTITY)->default(null)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(ItemEnum::TABLE_NAME, function (Blueprint $table) {
            $table->dropColumn(ItemEnum::SIZE);
            $table->dropColumn(ItemEnum::QUANTITY);
        });
    }
}
