<?php

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertisingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AdvertisingEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->text(AdvertisingEnum::BANNER_TITLE);
            $table->timestamp(AdvertisingEnum::BANNER_EXPIRE_DATE);
            $table->text(AdvertisingEnum::BANNER_IMAGE);
            $table->text(AdvertisingEnum::BANNER_DESCRIPTION);

            $table->text(AdvertisingEnum::ADVERT_PAGE_TITLE);
            $table->longText(AdvertisingEnum::ADVERT_PAGE_DESCRIPTION);
            $table->text(AdvertisingEnum::META_DESCRIPTION);
            $table->text(AdvertisingEnum::META_TAG);
            $table->integer(AdvertisingEnum::ADVERT_PAGE_DISCOUNT);

            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AdvertisingEnum::TABLE_NAME);
    }
}
