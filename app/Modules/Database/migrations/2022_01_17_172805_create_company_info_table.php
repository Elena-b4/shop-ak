<?php

use App\Modules\Database\src\Enums\CompanyInfoEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompanyInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CompanyInfoEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(CompanyInfoEnum::PHONES);
            $table->string(CompanyInfoEnum::EMAILS);
            $table->text(CompanyInfoEnum::DESCRIPTION);
            $table->text(CompanyInfoEnum::ADDRESS);
            $table->text(CompanyInfoEnum::NAME);
            $table->text(CompanyInfoEnum::META_DESCRIPTION);
            $table->text(CompanyInfoEnum::META_TAG);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CompanyInfoEnum::TABLE_NAME);
    }
}
