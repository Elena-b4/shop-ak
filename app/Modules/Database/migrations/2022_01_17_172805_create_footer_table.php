<?php

use App\Modules\Database\src\Enums\FooterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFooterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FooterEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(FooterEnum::NAME);
            $table->string(FooterEnum::URL);
            $table->string(MainEnum::SLUG);
            $table->boolean(MainEnum::VISIBLE)->default(0);
            $table->integer(MainEnum::SORT)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(FooterEnum::TABLE_NAME);
    }
}
