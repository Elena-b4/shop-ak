<?php

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdvertPageBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AdvertisingEnum::ADVERT_PAGE_BLOCK_TABLE, function (Blueprint $table) {
            $table->id();
            $table->string(AdvertisingEnum::ADVERT_PAGE_BLOCK_SLUG);
            $table->longText(AdvertisingEnum::DESCRIPTION);
            $table->foreignId(AdvertisingEnum::ADVERT_PAGE_ID)->references('id')->on(AdvertisingEnum::TABLE_NAME)->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(AdvertisingEnum::ADVERT_PAGE_BLOCK_TABLE);
    }
}
