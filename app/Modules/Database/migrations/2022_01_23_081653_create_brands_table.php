<?php

use App\Modules\Database\src\Enums\BrandEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(BrandEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->text(BrandEnum::NAME);
            $table->text(BrandEnum::COUNTRY);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(BrandEnum::TABLE_NAME);
    }
}
