<?php

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ItemEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')->on(ProductEnum::TABLE_NAME);
            $table->string(ItemEnum::VENDOR_CODE);
            $table->double(ItemEnum::PRICE)->nullable();
            $table->string(ItemEnum::DISCOUNT)->nullable();
            $table->boolean(ItemEnum::NEED_ORDER)->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ItemEnum::TABLE_NAME);
    }
}
