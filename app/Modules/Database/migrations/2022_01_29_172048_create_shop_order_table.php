<?php

use App\Modules\Database\src\Enums\ShopOrderEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ShopOrderEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->string(ShopOrderEnum::STATUS);
            $table->string(ShopOrderEnum::DESCRIPTION)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ShopOrderEnum::TABLE_NAME);
    }
}
