<?php

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ShopOrderEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemShopOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PivotTableEnum::ITEM_SHOP_ORDER, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ItemEnum::ITEM_ID)->references('id')->on(ItemEnum::TABLE_NAME);
            $table->foreignId(ShopOrderEnum::SHOP_ORDER_ID)->references('id')->on(ShopOrderEnum::TABLE_NAME);
            $table->integer(PivotTableEnum::COUNT)->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PivotTableEnum::ITEM_SHOP_ORDER);
    }
}
