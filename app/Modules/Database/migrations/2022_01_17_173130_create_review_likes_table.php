<?php

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ReviewEnum;
use App\Modules\Database\src\Enums\ReviewLikeEnum;
use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ReviewLikeEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ReviewEnum::REVIEW_ID)->references('id')->on(ReviewEnum::TABLE_NAME);
            $table->foreignId(UserEnum::USER_ID)->references('id')->on(UserEnum::TABLE_NAME);
            $table->boolean(MainEnum::VALUE);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ReviewLikeEnum::TABLE_NAME);
    }
}
