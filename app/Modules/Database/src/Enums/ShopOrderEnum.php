<?php

namespace App\Modules\Database\src\Enums;

class ShopOrderEnum
{

    public const TABLE_NAME = 'shop_order';
    public const STATUS = 'status';
    public const DESCRIPTION = 'description';
    public const SHOP_ORDER_ID = 'shop_order_id';
    public const STATUS_PREPARE_ORDER = 'prepare';

}
