<?php


namespace App\Modules\Database\src\Enums;


class FilterValueEnum
{
    public const TABLE_NAME = 'filter_values';

    public const LANG_GROUP = self::TABLE_NAME . '.';

    public const FILTER_VALUE_ID = 'filter_value_id';
}
