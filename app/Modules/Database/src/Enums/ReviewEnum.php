<?php


namespace App\Modules\Database\src\Enums;


class ReviewEnum
{
    public const TABLE_NAME = 'reviews';
    public const USER_NAME = 'user_name';
    public const USER_EMAIL = 'user_email';
    public const CONTENT = 'content';
    public const RATING = 'rating';
    public const APPROVED = 'approved';
    public const PRODUCT_UUID = 'product_uuid';

    public const REVIEW_ID = 'review_id';
}
