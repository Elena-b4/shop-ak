<?php


namespace App\Modules\Database\src\Enums;


class DeparturePointEnum
{
    public const TABLE_NAME = 'departure_points';
    public const NAME = 'name';
    public const ADDRESS = 'address';
    public const TIME_WORKS = 'time_works';
    public const DAY_WORKS = 'day_works';

    public const DEPARTURE_POINT_ID = 'departure_point_id';
}
