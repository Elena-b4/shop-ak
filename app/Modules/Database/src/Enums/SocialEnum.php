<?php


namespace App\Modules\Database\src\Enums;


class SocialEnum
{
    public const TABLE_NAME = 'socials';
    public const ICON = 'icon';
    public const URL = 'url';
    public const NAME = 'name';
}
