<?php


namespace App\Modules\Database\src\Enums;


class DiscountLevelEnum
{
    public const TABLE_NAME = 'discount_levels';
    public const DISCOUNT_AMOUNT = 'discount_amount';
}
