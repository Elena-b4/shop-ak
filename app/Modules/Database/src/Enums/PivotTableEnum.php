<?php


namespace App\Modules\Database\src\Enums;


class PivotTableEnum
{
    const PRODUCT_FILTERS = 'product_filters';
    const PRODUCT_FILTER_VALUES = 'product_filter_values';
    const PRODUCT_CATEGORIES = 'product_categories';
    const ITEM_DEPARTURE_POINT = 'item_departure_points';
    const ITEM_SHOP_ORDER = 'item_shop_order';
    const COUNT = 'count';

}
