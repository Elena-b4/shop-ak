<?php


namespace App\Modules\Database\src\Enums;


class CompanyInfoEnum
{
    public const TABLE_NAME = 'company_info';
    public const PHONES = 'phones';
    public const EMAILS = 'emails';
    public const DESCRIPTION = 'description';
    public const ADDRESS = 'address';
    public const NAME = 'name';
    public const META_DESCRIPTION = 'meta_description';
    public const META_TAG = 'meta_tag';

    public const COMPANY_INFO_ID = 1;

}
