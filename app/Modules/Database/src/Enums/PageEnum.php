<?php


namespace App\Modules\Database\src\Enums;


class PageEnum
{
    public const TABLE_NAME = 'pages';
    public const IMAGE = 'image';
    public const TEXT = 'text';
    public const META_DESCRIPTION = 'meta_description';
    public const META_TAG = 'meta_tag';
    public const LANG_GROUP = self::TABLE_NAME . '.';

}
