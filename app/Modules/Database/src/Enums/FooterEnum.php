<?php


namespace App\Modules\Database\src\Enums;


class FooterEnum
{
    public const TABLE_NAME = 'footer';
    public const NAME = 'name';
    public const URL = 'url';
}
