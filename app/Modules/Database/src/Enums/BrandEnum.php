<?php

namespace App\Modules\Database\src\Enums;

class BrandEnum
{

    public const TABLE_NAME = 'brands';
    public const NAME = 'name';
    public const COUNTRY = 'country';

}
