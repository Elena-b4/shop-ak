<?php


namespace App\Modules\Database\src\Enums;


class MenuEnum
{
    public const TABLE_NAME = 'menu';
    public const URL = 'url';
    public const MENU_ID = 'menu_id';
}
