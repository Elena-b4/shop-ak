<?php


namespace App\Modules\Database\src\Enums;


class UserEnum
{
    public const TABLE_NAME = 'users';
    public const NAME = 'name';
    public const PHONE = 'phone';
    public const EMAIL = 'email';
    public const EMAIL_VERIFIED_AT = 'email_verified_at';
    public const PHONE_VERIFIED_AT = 'phone_verified_at';
    public const PASSWORD = 'password';
    public const DISCOUNT_POINTS = 'discount_points';
    public const USER_ID = 'user_id';
    public const TABLE_NAME_SMS_CODES = 'sms_codes';
    public const CODE = 'code';
}
