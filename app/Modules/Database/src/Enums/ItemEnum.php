<?php


namespace App\Modules\Database\src\Enums;


class ItemEnum
{
    public const TABLE_NAME = 'items';

    public const VENDOR_CODE = 'vendor_code';
    public const PRICE = 'price';
    public const DISCOUNT = 'discount';
    public const NEED_ORDER = 'need_order';
    public const SIZE = 'size';
    public const QUANTITY = 'quantity';
    public const ITEM_ID = 'item_id';
}
