<?php


namespace App\Modules\Database\src\Enums;


class ItemsCountEnum
{
    public const TABLE_NAME = 'items_count';
    public const COUNT = 'count';
}
