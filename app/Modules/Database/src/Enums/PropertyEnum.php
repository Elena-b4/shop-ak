<?php


namespace App\Modules\Database\src\Enums;


class PropertyEnum
{
    public const TABLE_NAME = 'properties';

    public const LANG_GROUP = 'properties.';
    public const FA_ICON = 'fa-wpforms';

    public const PROPERTY_ID = 'property_id';
}
