<?php

namespace App\Modules\Database\src\Enums;

class RequestMessages
{
    public const USER_NOT_EXISTS = 'User not exists';
    public const INCORRECT_PASSWORD = 'Incorrect password';
    public const NOT_FOUND = 'Not found';
    public const USER_ALREADY_EXISTS = 'User already exists';
    public const ALREADY_EXISTS = 'Already exists';
}
