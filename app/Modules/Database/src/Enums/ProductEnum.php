<?php


namespace App\Modules\Database\src\Enums;


class ProductEnum
{
    public const TABLE_NAME = 'products';
    public const TABLE_NAME_NAMES = 'product_names';
    public const NAME = 'name';
    public const SHORT_DESCRIPTION = 'short_description';
    public const DESCRIPTION = 'description';
    public const PRICE = 'price';
    public const META_DESCRIPTION = 'meta_description';
    public const HIT = 'hit';
    public const NEW = 'is_new';
    public const META_TAG = 'meta_tag';
    public const BRAND_ID = 'brand_id';
    public const POPULAR = 'popular';
    public const COLOR = 'color';
    public const LANG = 'lang';
    public const VENDOR_CODE = 'vendor_code';
    public const UUID = 'uuid';
    public const MODEL = 'Product';

    public const PRODUCT_ID = 'product_id';
}
