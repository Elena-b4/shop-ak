<?php


namespace App\Modules\Database\src\Enums;


class OrderEnum
{
    public const TABLE_NAME = 'orders';

    public const COUNT = 'count';
    public const ADMIN_USER_ID = 'admin_user_id';

    public const DELIVERY_ADDRESS = 'delivery_address';
    public const DEPARTURE_POINT_ID = 'departure_point_id';
    public const PAYMENT_TYPE = 'payment_type';
    public const PAYMENT_STATUS = 'payment_status';
    public const PAYMENT_DATE = 'payment_date';
    public const DATE_DELIVERY = 'date_delivery';
    public const DISCOUNT_POINTS_AWARDED = 'discount_points_awarded';
    public const STATUS = 'status';
    public const DELETE_BY_USER = 'delete_by_user';

    public const ORDER_ID = 'order_id';

}
