<?php

namespace App\Modules\Database\src\Enums;

class NewProductEnum
{

    public const TABLE_NAME = 'new_products';
    public const URL = 'url';
    public const DESCRIPTION = 'description';
    public const PRICE = 'price';
    public const COUNT = 'count';

}
