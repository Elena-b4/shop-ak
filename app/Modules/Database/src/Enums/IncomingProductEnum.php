<?php


namespace App\Modules\Database\src\Enums;


class IncomingProductEnum
{
    public const TABLE_NAME = 'incoming_products';
    public const COUNT = 'count';
    //@TODO изменить на связя с другой таблицей, найти похожие места
    public const ADMIN_USER_ID = 'admin_user_id';
    public const ADMIN_USERS = 'admin_users';
}
