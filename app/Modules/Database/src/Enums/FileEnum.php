<?php


namespace App\Modules\Database\src\Enums;


class FileEnum
{
    public const TABLE_NAME = 'files';
    public const PATH = 'path';
    public const URL = 'url';
    public const TYPE = 'type';
    public const ENTITY = 'entity';
    public const ENTITY_ID = 'entity_id';
    public const NAME = 'name';
    public const MODEL = 'File';
}
