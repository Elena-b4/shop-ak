<?php


namespace App\Modules\Database\src\Enums;


class AdvertisingEnum
{
    public const TABLE_NAME = 'advertisings';

    public const BANNER_TITLE = 'banner_title';
    public const BANNER_EXPIRE_DATE = 'banner_expire_date';
    public const BANNER_IMAGE = 'banner_image';
    public const BANNER_DESCRIPTION = 'banner_description';

    public const ADVERT_PAGE_DISCOUNT = 'discount';

    public const ADVERT_PAGE_TITLE = 'advert_page_title';
    public const ADVERT_PAGE_DESCRIPTION = 'advert_page_description';

    public const ADVERT_PAGE_BLOCK_TABLE = 'advert_page_blocks';
    public const ADVERT_PAGE_BLOCK_ID = 'advert_page_block_id';
    public const ADVERT_PAGE_ID = 'advert_page_id';
    public const ADVERT_PAGE_BLOCK_SLUG = 'slug';
    public const DESCRIPTION = 'description';

    public const ADVERT_PAGE_PRODUCT_TABLE = 'advert_products';

    public const META_DESCRIPTION = 'meta_description';
    public const META_TAG = 'meta_tag';

    public const ADVERTISING_ID = 'advertising_id';
}
