<?php


namespace App\Modules\Database\src\Enums;


class ProductPropertyEnum
{
    public const TABLE_NAME = 'product_properties';

    public const LANG_GROUP = self::TABLE_NAME . '.';
}
