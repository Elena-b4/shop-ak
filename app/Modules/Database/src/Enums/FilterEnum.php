<?php


namespace App\Modules\Database\src\Enums;


class FilterEnum
{
    public const TABLE_NAME = 'filters';

    public const LANG_GROUP = self::TABLE_NAME . '.';

    public const FILTER_ID = 'filter_id';
}
