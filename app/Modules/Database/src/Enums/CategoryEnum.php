<?php


namespace App\Modules\Database\src\Enums;


class CategoryEnum
{
    public const TABLE_NAME = 'categories';
    public const ICON = 'icon';

    public const CATEGORY_ID = 'category_id';
}
