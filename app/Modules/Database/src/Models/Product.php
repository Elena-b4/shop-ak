<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\ProductPropertyEnum;
use App\Modules\Database\src\Enums\PropertyEnum;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Product extends Model
{
    use Searchable;
    use HasSlug;



    use ModelTree {
        ModelTree::boot as treeBoot;
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'description' => $this->description,
        ];
    }


    protected $table = ProductEnum::TABLE_NAME;

    protected $fillable = ['*'];

    protected $casts = [
        ProductEnum::DESCRIPTION => 'json',
        ProductEnum::SHORT_DESCRIPTION => 'json',
        ProductEnum::META_TAG => 'json',
        ProductEnum::META_DESCRIPTION => 'json',
        MainEnum::TITLE => 'json',
    ];


    protected $with = ['names'];

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
             ->saveSlugsTo('slug');
    }


    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'product_filters', ProductEnum::PRODUCT_ID, FilterEnum::FILTER_ID);
    }

    public function properties()
    {
        return $this->belongsToMany(Property::class, ProductPropertyEnum::TABLE_NAME, ProductEnum::PRODUCT_ID, PropertyEnum::PROPERTY_ID);
    }

    public function brands()
    {
        return $this->hasOne(Brand::class, 'id', ProductEnum::BRAND_ID);
    }

    public function values()
    {
        return $this->belongsToMany(FilterValue::class, PivotTableEnum::PRODUCT_FILTER_VALUES, ProductEnum::PRODUCT_ID, FilterValueEnum::FILTER_VALUE_ID);
    }

    public function names()
    {
        return $this->hasMany(ProductNames::class, ProductEnum::PRODUCT_ID, 'id');
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class, PivotTableEnum::PRODUCT_CATEGORIES, ProductEnum::PRODUCT_ID, CategoryEnum::CATEGORY_ID);
    }


    public function files()
    {
        return $this->hasMany(File::class, FileEnum::ENTITY_ID, 'id')->where('entity', ProductEnum::TABLE_NAME);
    }

    public function images()
    {
        return $this->hasMany(File::class, FileEnum::ENTITY_ID, 'id')->where('entity', ProductEnum::TABLE_NAME)->where(FileEnum::TYPE, 'img');
    }

    public function items()
    {
        return $this->hasMany(Item::class, ProductEnum::PRODUCT_ID, 'id');
    }


}
