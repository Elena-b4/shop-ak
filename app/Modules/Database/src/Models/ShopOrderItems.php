<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ShopOrderEnum;
use Illuminate\Database\Eloquent\Model;

class ShopOrderItems extends Model
{

    protected $table = PivotTableEnum::ITEM_SHOP_ORDER;

    protected $fillable = [ItemEnum::ITEM_ID, ShopOrderEnum::SHOP_ORDER_ID, PivotTableEnum::COUNT];

}
