<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PropertyEnum;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{

    protected $fillable = [MainEnum::SLUG, MainEnum::VISIBLE, MainEnum::SORT];

    protected $appends = ['title'];

    protected $with = ['values'];

    protected $table = PropertyEnum::TABLE_NAME;

    public function getTitleAttribute()
    {
        return trans(PropertyEnum::LANG_GROUP . $this->attributes[MainEnum::SLUG]);
    }

    public function values(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(ProductProperties::class, PropertyEnum::PROPERTY_ID);
    }

}
