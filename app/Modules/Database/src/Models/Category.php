<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\CategoryEnum;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class   Category extends Model
{
    use  AdminBuilder;

    use ModelTree {
        ModelTree::boot as treeBoot;
    }

    protected $appends = ['title'];

    protected $table = CategoryEnum::TABLE_NAME;

    protected $fillable = [MainEnum::PARENT_ID, MainEnum::SLUG, CategoryEnum::ICON, MainEnum::SORT, MainEnum::VISIBLE,];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn(MainEnum::PARENT_ID);
        $this->setOrderColumn(MainEnum::SORT);
        $this->setTitleColumn(MainEnum::SLUG);
    }


    public function children()
    {
        return $this->hasMany(Category::class, MainEnum::PARENT_ID)->with('children');
    }

    public function childrenApi()
    {
        return $this->hasMany(Category::class, MainEnum::PARENT_ID)
            ->with('childrenApi')->orderBy(MainEnum::SORT)
            ->select([CategoryEnum::ICON, 'id', MainEnum::PARENT_ID, MainEnum::SLUG]);
    }

    public function getTitleAttribute()
    {

        return __('category.' . $this->attributes[MainEnum::SLUG]);
    }


    public function filters()
    {
        return $this->belongsToMany(Filter::class, 'category_filters', CategoryEnum::CATEGORY_ID, FilterEnum::FILTER_ID);
    }


}
