<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{

    use  AdminBuilder;

    use ModelTree {
        ModelTree::boot as treeBoot;
    }

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setOrderColumn(MainEnum::SORT);
        $this->setTitleColumn(FileEnum::URL);
    }

    protected $table = FileEnum::TABLE_NAME;

    protected $fillable = [FileEnum::NAME,  MainEnum::SORT, MainEnum::VISIBLE, FileEnum::TYPE, FileEnum::ENTITY, FileEnum::ENTITY_ID];

}
