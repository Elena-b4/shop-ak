<?php


namespace App\Modules\Database\src\Models;


use App\Modules\Database\src\Enums\PivotTableEnum;
use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model
{
    protected $table = PivotTableEnum::PRODUCT_CATEGORIES;

}
