<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{

    protected $table = MenuEnum::TABLE_NAME;

    protected $fillable = [MainEnum::SLUG, MainEnum::SORT, MenuEnum::URL];

    protected $appends = ['title'];

    public function items()
    {
        return $this->hasMany(MenuItems::class, MenuEnum::MENU_ID);
    }

    public function getTitleAttribute()
    {
        return __('category.' . $this->attributes[MainEnum::SLUG]);
    }


}
