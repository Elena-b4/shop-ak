<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\CompanyInfoEnum;
use Illuminate\Database\Eloquent\Model;

class CompanyInfo extends Model
{

    protected $table = CompanyInfoEnum::TABLE_NAME;

    protected $casts = [
      CompanyInfoEnum::DESCRIPTION => 'json',
      CompanyInfoEnum::ADDRESS => 'json',
      CompanyInfoEnum::NAME => 'json',
      CompanyInfoEnum::META_DESCRIPTION => 'json',
      CompanyInfoEnum::META_TAG => 'json',
    ];

}
