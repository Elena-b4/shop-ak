<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\LikeEnum;
use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = LikeEnum::TABLE_NAME;
    protected $fillable = ['*'];

}
