<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Eloquent\Model;

class AdvertPageBlock extends Model
{
    protected $table = AdvertisingEnum::ADVERT_PAGE_BLOCK_TABLE;

    protected $fillable = [MainEnum::SLUG, AdvertisingEnum::DESCRIPTION];

    protected $casts = [
        AdvertisingEnum::DESCRIPTION => 'json',
    ];

    public function advert()
    {
        return $this->belongsTo(Advertising::class, 'id', AdvertisingEnum::ADVERT_PAGE_ID);
    }

    public function products()
    {
       return $this->belongsToMany(Product::class,'advert_products', AdvertisingEnum::ADVERT_PAGE_BLOCK_ID, ProductEnum::PRODUCT_ID);
    }

}
