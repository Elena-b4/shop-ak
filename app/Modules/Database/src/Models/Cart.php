<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\CartEnum;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    protected $fillable = ['*'];

    protected $table = CartEnum::TABLE_NAME;

}
