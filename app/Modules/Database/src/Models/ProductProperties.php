<?php


namespace App\Modules\Database\src\Models;


use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PropertyEnum;
use Illuminate\Database\Eloquent\Model;

class ProductProperties extends Model
{

    protected $fillable = [PropertyEnum::PROPERTY_ID, PropertyEnum::PROPERTY_ID, MainEnum::VALUE];

    public function property()
    {
        return $this->belongsTo(Property::class);
    }

}
