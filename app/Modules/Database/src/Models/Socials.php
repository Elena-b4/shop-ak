<?php

namespace App\Modules\Database\src\Models;


use App\Modules\Database\src\Enums\SocialEnum;
use Illuminate\Database\Eloquent\Model;

class Socials extends Model
{
    protected $table = SocialEnum::TABLE_NAME;

    protected $fillable = ['*'];

    protected $casts = [
        'name' =>'json',
    ];

}
