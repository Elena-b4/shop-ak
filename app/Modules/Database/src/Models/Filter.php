<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Eloquent\Model;

class Filter extends Model
{

    protected $fillable = [MainEnum::SLUG, MainEnum::SORT, MainEnum::VISIBLE];

    protected $appends = ['title'];

    protected $with = ['values'];

    protected $table = FilterEnum::TABLE_NAME;

    public function getTitleAttribute()
    {
        return trans(FilterEnum::LANG_GROUP . $this->attributes[MainEnum::SLUG]);
    }

    public function values(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(FilterValue::class, FilterEnum::FILTER_ID);
    }

}
