<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\AdvertisingEnum;
use Illuminate\Database\Eloquent\Model;

class Advertising extends Model
{
    protected $table = AdvertisingEnum::TABLE_NAME;
    protected $fillable = ['*'];
    protected $casts = [
        AdvertisingEnum::BANNER_TITLE => 'json',
//        AdvertisingEnum::BANNER_EXPIRE_DATE => 'json',
        AdvertisingEnum::BANNER_DESCRIPTION => 'json',
        AdvertisingEnum::ADVERT_PAGE_TITLE => 'json',
        AdvertisingEnum::ADVERT_PAGE_DESCRIPTION => 'json',
        AdvertisingEnum::META_TAG => 'json',
        AdvertisingEnum::META_DESCRIPTION => 'json',
    ];

    public function pageBlocks()
    {
        return $this->hasMany(AdvertPageBlock::class, AdvertisingEnum::ADVERT_PAGE_ID, 'id');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($advert) {
            $advert->pageBlocks()->delete();
        });
    }

}
