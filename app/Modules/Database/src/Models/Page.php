<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PageEnum;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;


class Page extends Model
{
    use Sluggable;

    protected $table = PageEnum::TABLE_NAME;

    protected $guarded = [];

    protected $casts = [
        PageEnum::TEXT => 'json',
        PageEnum::META_TAG => 'json',
        PageEnum::META_DESCRIPTION => 'json',
        MainEnum::TITLE => 'json',
    ];

    public function sluggable(): array
    {
        return [
            MainEnum::SLUG => [
                'separator' => '_',
                'unique' => true,
                'uniqueSuffix' => null,
                'firstUniqueSuffix' => 2,
                'includeTrashed' => false,
                'reserved' => null,
                'maxLength' => 20,
                'maxLengthKeepWords' => true,
            ]
        ];
    }

}
