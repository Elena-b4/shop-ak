<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Eloquent\Model;

class SmsCode extends Model
{
    protected $table = UserEnum::TABLE_NAME_SMS_CODES;
    protected $fillable = ['*'];
}
