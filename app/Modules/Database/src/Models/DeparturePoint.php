<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\DeparturePointEnum;
use Illuminate\Database\Eloquent\Model;

class DeparturePoint extends Model
{

    protected $table = DeparturePointEnum::TABLE_NAME;
    protected $casts = [DeparturePointEnum::DAY_WORKS => 'json'];

}
