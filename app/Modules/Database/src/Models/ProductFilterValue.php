<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class ProductFilterValue extends Model
{

    use  AdminBuilder;

    use ModelTree {
        ModelTree::boot as treeBoot;
    }

    protected $table = PivotTableEnum::PRODUCT_FILTER_VALUES;

    protected $fillable = [ProductEnum::PRODUCT_ID, FilterValueEnum::FILTER_VALUE_ID];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn(MainEnum::PARENT_ID);
        $this->setOrderColumn(MainEnum::SORT);
        $this->setTitleColumn(MainEnum::SLUG);
    }


    public function children()
    {
        return $this->hasMany(ProductFilterValue::class, MainEnum::PARENT_ID)->with('children');
    }

    public function filters()
    {
        return $this->belongsToMany(Filter::class, FilterValueEnum::TABLE_NAME,
            'id', 'filter_id', 'filter_value_id', 'id');
    }


}
