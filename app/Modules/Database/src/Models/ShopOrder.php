<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\ShopOrderEnum;
use Illuminate\Database\Eloquent\Model;

class ShopOrder extends Model
{

    protected $table = ShopOrderEnum::TABLE_NAME;

    protected $fillable = [ShopOrderEnum::STATUS, ShopOrderEnum::DESCRIPTION];

    public function items()
    {
        return $this->hasMany(ShopOrderItems::class, ShopOrderEnum::SHOP_ORDER_ID);
    }

}
