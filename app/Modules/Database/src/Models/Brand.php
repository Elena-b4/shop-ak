<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\BrandEnum;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    protected $table = BrandEnum::TABLE_NAME;

    protected $fillable = [BrandEnum::COUNTRY, BrandEnum::NAME];

}
