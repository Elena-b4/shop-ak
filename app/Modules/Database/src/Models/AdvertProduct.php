<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\AdvertisingEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Eloquent\Model;

class AdvertProduct extends Model
{
    protected $table = AdvertisingEnum::ADVERT_PAGE_PRODUCT_TABLE;
    protected $fillable = ['advert_page_block_id', 'product_id'];

//    public function advertPageBlocks()
//    {
//        $this->belongsToMany(AdvertPageBlock::class, 'advert_products', ProductEnum::PRODUCT_ID,AdvertisingEnum::ADVERT_PAGE_BLOCK_ID);
//    }
}
