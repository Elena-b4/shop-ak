<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class ProductNames extends Model
{

    use Searchable;

    /**
     * Get the indexable data array for the model.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        return [
            'id' => $this->id,
            'product_id' => $this->product_id,
            'name' => $this->name,
        ];
    }

    protected $table = ProductEnum::TABLE_NAME_NAMES;

    protected $fillable = [ProductEnum::PRODUCT_ID, ProductEnum::NAME, MainEnum::LANG];

}
