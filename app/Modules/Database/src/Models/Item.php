<?php

namespace App\Modules\Database\src\Models;

use App\Modules\Api\src\Models\FileApi;
use App\Modules\Database\src\Enums\DeparturePointEnum;
use App\Modules\Database\src\Enums\FileEnum;
use App\Modules\Database\src\Enums\ItemEnum;
use App\Modules\Database\src\Enums\PivotTableEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $table = ItemEnum::TABLE_NAME;

    protected $fillable = [ItemEnum::PRICE, ProductEnum::PRODUCT_ID, ItemEnum::SIZE, ItemEnum::DISCOUNT];

    public function images()
    {
        return $this->hasMany(FileApi::class, FileEnum::ENTITY_ID, 'id')->where('entity', ItemEnum::TABLE_NAME);
    }

    public function points()
    {
        return $this->belongsToMany(DeparturePoint::class, PivotTableEnum::ITEM_DEPARTURE_POINT, ItemEnum::ITEM_ID, DeparturePointEnum::DEPARTURE_POINT_ID);
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', ProductEnum::PRODUCT_ID);
    }


}
