<?php


namespace App\Modules\Database\src\Models;


use App\Modules\Api\src\Models\FilterApi;
use App\Modules\Database\src\Enums\FilterEnum;
use App\Modules\Database\src\Enums\FilterValueEnum;
use App\Modules\Database\src\Enums\MainEnum;
use Illuminate\Database\Eloquent\Model;

class FilterValue extends Model
{

    protected $table = FilterValueEnum::TABLE_NAME;

    protected $fillable = [FilterEnum::FILTER_ID, MainEnum::SLUG, MainEnum::SORT];

    protected $appends = ['title'];

    public function getTitleAttribute()
    {
        return trans(FilterValueEnum::LANG_GROUP . $this->attributes[MainEnum::SLUG]);
    }

    public function filter()
    {
        return $this->belongsTo(FilterApi::class, 'id', FilterEnum::FILTER_ID);
    }

}
