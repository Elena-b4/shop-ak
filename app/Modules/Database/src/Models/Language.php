<?php

namespace App\Modules\Database\src\Models;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'language_lines';

    protected $casts = [
        'text' =>'json',
    ];
}

