<?php

namespace App\Modules\Database\src\Models;


use App\Modules\Database\src\Enums\MainEnum;
use App\Modules\Database\src\Enums\MenuEnum;
use App\Modules\Database\src\Enums\MenuItemEnum;
use Encore\Admin\Traits\AdminBuilder;
use Encore\Admin\Traits\ModelTree;
use Illuminate\Database\Eloquent\Model;

class MenuItems extends Model
{

    use  AdminBuilder;

    use ModelTree {
        ModelTree::boot as treeBoot;
    }

    protected $appends = ['title'];

    protected $table = MenuItemEnum::TABLE_NAME;

    protected $fillable = [MainEnum::SLUG, MainEnum::SORT, MainEnum::PARENT_ID, MenuEnum::URL, MenuEnum::MENU_ID];


    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->setParentColumn(MainEnum::PARENT_ID);
        $this->setOrderColumn(MainEnum::SORT);
        $this->setTitleColumn(MainEnum::SLUG);
    }


    public function children()
    {
        return $this->hasMany(MenuItems::class, MainEnum::PARENT_ID)->with('children');
    }

    /**
     * Get options for Select field in form.
     *
     * @param \Closure|null $closure
     * @param string $rootText
     *
     * @return array
     */
    public static function selectOptions(\Closure $closure = null, $rootText = 'ROOT')
    {
        $options = (new static())->where()->withQuery($closure)->buildSelectOptions();

        return collect($options)->prepend($rootText, 0)->all();
    }

    public function getTitleAttribute()
    {
        return trans('category.' . $this->attributes[MainEnum::SLUG]);
    }
}
