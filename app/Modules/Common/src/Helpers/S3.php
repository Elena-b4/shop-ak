<?php

namespace App\Modules\Common\src\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class S3
{
    public static function getUrl($url)
    {
        if ($url != null) {
            return Storage::disk('s3')->temporaryUrl($url, Carbon::now()->addMinutes(5));

        }
            return false;
    }
}
