<?php


namespace App\Modules\Common\src\Helpers;


class Mail
{
    public const RESET_PASSWORD_SUBJECT = 'Reset Password';
    public const TEST_SUBJECT = 'Test';

    public const TEST_TEMPLATE = 'd-6d8a267c76c144afaa571e6f57376cec';
    public const RESET_PASSWORD_TEMPLATE = 'd-b3370fe184094c3294ae63093971f818';
}
