<?php


namespace App\Modules\Common\src\services;

use SendGrid\Mail\Mail;
use App\Modules\Common\src\Helpers\Mail as MailHelper;
use Sichikawa\LaravelSendgridDriver\SendGrid;

class MailService
{
    use SendGrid;

    public function sendMail($subject, $to, $params)
    {
        $email = new Mail();
        $email->setFrom(config('mail.from.address'));
        $email->setSubject($subject);
        $email->setTemplateId(MailHelper::TEST_TEMPLATE);
        $email->addTo($to);
        $email->addDynamicTemplateDatas([
            'name' => $params['name']
        ]);

        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
        try {
            $sendgrid->send($email);
            return response('Message was sent successfully!');
        } catch (\Exception $e) {
            return 'Caught exception: ' . $e->getMessage() . "/n";
        }
    }

    public function sendTokenForResetPassword($subject, $to, $params)
    {
        $email = new Mail();
        $email->setFrom(config('mail.from.address'));
        $email->setSubject($subject);
        $email->addTo($to);
        $email->addDynamicTemplateDatas([
            'url' => $params['url']
        ]);

        $email->setTemplateId(MailHelper::RESET_PASSWORD_TEMPLATE);

        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
        try {
            $sendgrid->send($email);
        } catch (\Exception $e) {
            return response('Caught exception: ' . $e->getMessage() . "/n");
        }
    }
}
