<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            AdminSeeder::class,
        ]);

        /*
        DB::unprepared(file_get_contents('data/shop_db_public_brands.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_filters.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_filter_values.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_categories.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_category_filters.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_company_info.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_files.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_language_lines.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_menu.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_menu_items.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_products.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_properties.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_items.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_product_properties.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_product_categories.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_product_filters.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_product_filter_values.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_product_names.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_socials.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_company_info.sql'));
        DB::unprepared(file_get_contents('data/shop_db_public_advertisings.sql'));
        DB::unprepared(file_get_contents('data/clear_index.sql'));
        */

        $sqlFiles = File::glob('data/*.sql');

        foreach ($sqlFiles as $sqlFile) {
            DB::unprepared(file_get_contents($sqlFile));
        }
    }
}
