<?php

namespace Database\Seeders;

use App\Modules\Database\src\Enums\PropertyEnum;
use App\Modules\Database\src\Models\Advertising;
use App\Modules\Database\src\Models\Brand;
use App\Modules\Database\src\Models\Category;
use App\Modules\Database\src\Models\CompanyInfo;
use App\Modules\Database\src\Models\Filter;
use App\Modules\Database\src\Models\FilterValue;
use Encore\Admin\Auth\Database\Administrator;
use Encore\Admin\Auth\Database\Menu;
use Encore\Admin\Auth\Database\Permission;
use Encore\Admin\Auth\Database\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    public function run()
    {
        // create a user.
        Administrator::truncate();
        Administrator::create([
            'username' => 'admin',
            'password' => Hash::make('admin'),
            'name' => 'Administrator',
        ]);

        // create a role.
        Role::truncate();
        Role::create([
            'name' => 'Administrator',
            'slug' => 'administrator',
        ]);

        // add role to user.
        Administrator::first()->roles()->save(Role::first());

        //create a permission
        Permission::truncate();
        Permission::insert([
            [
                'name' => 'All permission',
                'slug' => '*',
                'http_method' => '',
                'http_path' => '*',
            ],
            [
                'name' => 'Dashboard',
                'slug' => 'dashboard',
                'http_method' => 'GET',
                'http_path' => '/',
            ],
            [
                'name' => 'Login',
                'slug' => 'auth.login',
                'http_method' => '',
                'http_path' => "/auth/login\r\n/auth/logout",
            ],
            [
                'name' => 'User setting',
                'slug' => 'auth.setting',
                'http_method' => 'GET,PUT',
                'http_path' => '/auth/setting',
            ],
            [
                'name' => 'Auth management',
                'slug' => 'auth.management',
                'http_method' => '',
                'http_path' => "/auth/roles\r\n/auth/permissions\r\n/auth/menu\r\n/auth/logs",
            ],
        ]);

        Role::first()->permissions()->save(Permission::first());

        // add default menus.
        Menu::truncate();
        Menu::insert([
            [
                'parent_id' => 0,
                'order' => 0,
                'title' => 'Products',
                'icon' => 'fa-product-hunt',
                'uri' => '/product-common',
            ],
            [
                'parent_id' => 0,
                'order' => 0,
                'title' => 'Filters',
                'icon' => 'fa-filter',
                'uri' => '/filters-common',
            ],
            [
                'parent_id' => 0,
                'order' => 0,
                'title' => 'Company',
                'icon' => 'fa-info',
                'uri' => '/company-common',
            ],
            [
                'parent_id' => 0,
                'order' => 0,
                'title' => 'Advert',
                'icon' => 'fa-percent',
                'uri' => '/advertising',
            ],
            [
                'parent_id' => 0,
                'order' => 1,
                'title' => 'Dashboard',
                'icon' => 'fa-bar-chart',
                'uri' => '/',
            ],
            [
                'parent_id' => 0,
                'order' => 2,
                'title' => 'Admin',
                'icon' => 'fa-tasks',
                'uri' => '',
            ],

        ]);
        Menu::insert([
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Products')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Products',
                'icon' => 'fa-product-hunt',
                'uri' => '/product',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Products')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Categories',
                'icon' => 'fa-bars',
                'uri' => '/categories',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Products')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Brands',
                'icon' => 'fa-barcode',
                'uri' => '/brand',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Filters')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Filters',
                'icon' => 'fa-filter',
                'uri' => '/filters',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Filters')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Properties',
                'icon' => PropertyEnum::FA_ICON,
                'uri' => '/properties',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Company')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Departure points',
                'icon' => 'fa-info',
                'uri' => '/departure-points',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Company')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Socials',
                'icon' => 'fa-share-alt',
                'uri' => '/socials',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Company')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Company info',
                'icon' => 'fa-info',
                'uri' => '/company',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Company')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Departure points',
                'icon' => 'fa-map-pin',
                'uri' => '/departure-points',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Company')->get('id')[0]->id,
                'order' => 0,
                'title' => 'Languages',
                'icon' => 'fa-globe',
                'uri' => '/lang',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Dashboard')->get('id')[0]->id,
                'order' => 3,
                'title' => 'Users',
                'icon' => 'fa-users',
                'uri' => 'auth/users',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Dashboard')->get('id')[0]->id,
                'order' => 4,
                'title' => 'Roles',
                'icon' => 'fa-user',
                'uri' => 'auth/roles',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Dashboard')->get('id')[0]->id,
                'order' => 5,
                'title' => 'Permission',
                'icon' => 'fa-ban',
                'uri' => 'auth/permissions',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Dashboard')->get('id')[0]->id,
                'order' => 6,
                'title' => 'Menu',
                'icon' => 'fa-bars',
                'uri' => 'auth/menu',
            ],
            [
                'parent_id' => DB::table('admin_menu')->where('title', 'Dashboard')->get('id')[0]->id,
                'order' => 7,
                'title' => 'Operation log',
                'icon' => 'fa-history',
                'uri' => 'auth/logs',
            ],
        ]);

        // add role to menu.
        Menu::find(2)->roles()->save(Role::first());

        Brand::insert([
            [
                'name' => 'AK',
                'country' => 'Belarus',
            ],
        ]);

        Advertising::insert([
            [
                'banner_title' => '{"Рeклама тест 1":"sdf","banner_titleen":"Advertising test 1"}',
                'banner_expire_date' => (new \DateTime('now'))->add(new \DateInterval('P2W')),
                'banner_image' => 'files/advertising/img.png',
                'banner_description' => '{"banner_descriptionru":"<p>Реклама тест 1 описание баннер<\/p>","banner_descriptionen":"<p>Advertising test 1 description banner<\/p>"}',
                'advert_page_title' => '{"advert_page_titleru":"Название реклама 1","advert_page_titleen":"Advertising 1 title"}',
                'advert_page_description' => '{"advert_page_descriptionru":"<p>Реклама тест 1 описание страница<\/p>","advert_page_descriptionen":"<p>Advertising test 1 description page<\/p>"}',
                'meta_description' => '{"meta_descriptionru":"мета описание реклама 1","meta_descriptionen":"meta description advert 1"}',
                'meta_tag' => '{"meta_tagru":"мета тег реклама 1","meta_tagen":"meta tag advert 1"}',
                'discount' => 10,
                'visible' => true,
            ],
            [
                'banner_title' => '{"Рeклама тест 2":"sdf","banner_titleen":"Advertising test 2"}',
                'banner_expire_date' => (new \DateTime('now'))->add(new \DateInterval('P2W')),
                'banner_image' => 'files/advertising/img_1.png',
                'banner_description' => '{"banner_descriptionru":"<p>Реклама тест 2 описание баннер<\/p>","banner_descriptionen":"<p>Advertising test 2 description banner<\/p>"}',
                'advert_page_title' => '{"advert_page_titleru":"Название реклама 2","advert_page_titleen":"Advertising 2 title"}',
                'advert_page_description' => '{"advert_page_descriptionru":"<p>Реклама тест 2 описание страница<\/p>","advert_page_descriptionen":"<p>Advertising test 2 description page<\/p>"}',
                'meta_description' => '{"meta_descriptionru":"мета описание реклама 2","meta_descriptionen":"meta description advert 2"}',
                'meta_tag' => '{"meta_tagru":"мета тег реклама 2","meta_tagen":"meta tag advert 2"}',
                'discount' => 7,
                'visible' => true,
            ],
            [
                'banner_title' => '{"Рeклама тест 3":"sdf","banner_titleen":"Advertising test 3"}',
                'banner_expire_date' => (new \DateTime('now'))->add(new \DateInterval('P2W')),
                'banner_image' => 'files/advertising/img_2.png',
                'banner_description' => '{"banner_descriptionru":"<p>Реклама тест 3 описание баннер<\/p>","banner_descriptionen":"<p>Advertising test 3 description banner<\/p>"}',
                'advert_page_title' => '{"advert_page_titleru":"Название реклама 3","advert_page_titleen":"Advertising 3 title"}',
                'advert_page_description' => '{"advert_page_descriptionru":"<p>Реклама тест 3 описание страница<\/p>","advert_page_descriptionen":"<p>Advertising test 3 description page<\/p>"}',
                'meta_description' => '{"meta_descriptionru":"мета описание реклама 3","meta_descriptionen":"meta description advert 3"}',
                'meta_tag' => '{"meta_tagru":"мета тег реклама 3","meta_tagen":"meta tag advert 3"}',
                'discount' => 15,
                'visible' => true,
            ],
        ]);

        Category::insert([
            [
                'slug' => 'all',
                'parent_id' => 0,
                'sort' => 1,
                'visible' => true,
            ],
            [
                'slug' => 'very_sexy',
                'parent_id' => 0,
                'sort' => 2,
                'visible' => true,
            ],
            [
                'slug' => 'base',
                'parent_id' => 0,
                'sort' => 3,
                'visible' => true,
            ],
            [
                'slug' => 'corsets',
                'parent_id' => 0,
                'sort' => 4,
                'visible' => true,
            ],
            [
                'slug' => 'body',
                'parent_id' => 5,
                'sort' => 3,
                'visible' => true,
            ],
        ]);

        CompanyInfo::insert([
            [
                'phones' => '+375 29 123 45 67; +375 44 222 33 44',
                'emails' => 'omg@big.shop',
                'description' => '{"descriptionru":"опсиание компании [ru]","descriptionen":"company description [en]"}',
                'address' => '{"addressru":"адрес компании [ru]","addressen":"company address [en]"}',
                'name' => '{"nameru":"название компании [ru]","nameen":"company name [en]"}',
                'meta_description' => '{"meta_descriptionru":"мета описание компании [ru]","meta_descriptionen":"company meta description [en]"}',
                'meta_tag' => '{"meta_tagru":"мета теги компании [ru]","meta_tagen":"company meta tag [en]"',
            ]
        ]);

        Filter::insert([
            [
                'slug' => 'product_type',
                'sort' => 1,
                'visible' => true,
            ],
            [
                'slug' => 'price',
                'sort' => 2,
                'visible' => true,
            ],
            [
                'slug' => 'bra_fullness',
                'sort' => 3,
                'visible' => true,
            ],
            [
                'slug' => 'bra_underbust_volume',
                'sort' => 4,
                'visible' => true,
            ],
            [
                'slug' => 'bra_model',
                'sort' => 5,
                'visible' => true,
            ],
            [
                'slug' => 'panties_model',
                'sort' => 6,
                'visible' => true,
            ],
            [
                'slug' => 'panties_size',
                'sort' => 7,
                'visible' => true,
            ],
            [
                'slug' => 'colour',
                'sort' => 8,
                'visible' => true,
            ],
        ]);
    }
}
