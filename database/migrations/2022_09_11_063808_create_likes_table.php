<?php

use App\Modules\Database\src\Enums\LikeEnum;
use App\Modules\Database\src\Enums\ProductEnum;
use App\Modules\Database\src\Enums\UserEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(LikeEnum::TABLE_NAME, function (Blueprint $table) {
            $table->id();
            $table->foreignId(ProductEnum::PRODUCT_ID)->references('id')
                ->on(ProductEnum::TABLE_NAME)->onDelete('cascade');
            $table->foreignId(UserEnum::USER_ID)->references('id')
                ->on(UserEnum::TABLE_NAME)->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('likes');
    }
}
